import pandas as pd
import numpy as np
import sys
import argparse,os
from os import listdir
import re
import warnings
import plotly
import plotly.graph_objs as go
from plotly.offline import plot
import plotly.plotly as py
import random

pd.set_option('mode.chained_assignment', None)
np.seterr(all='ignore')
warnings.filterwarnings("ignore")
#Runtime file headers
headers=['TaskID','submitTime','WaitTime','ExecStartTime','ExecFinalTime','ExecTime']
schedTraceHeaders=['date','time','instanceID']
powerClass = {}
powerClass_sets_three = {
                "A":["stratos-005","stratos-006"],
                "B":["stratos-007","stratos-008"],
                "C":["stratos-001","stratos-002","stratos-003","stratos-004"]
}
powerClass_sets_four = {
                "A":["stratos-005","stratos-006"],
                "B":["stratos-007"],
                "C":["stratos-008"],
                "D":["stratos-001","stratos-002","stratos-003","stratos-004"]
}

overall_wt_list=list()
overall_et_list=list()
exec_pertask_avg_map={}
turn_pertask_avg_map={}
var_map_list=list()
taskList=list()
wt_map_per_task={}
task_exec_map={}
task_turn_map={}
runtime_docs_list=list()
sched_docs_list=list()
pcp_docs_list=list()
task_average_map={}
file_names=list()
exec_times_map=dict()
turn_time_map=dict()
task_average_exec_map={}
task_average_turn_map={}
overall_list=list()
medpu_overall_wt_list=list()
medpu_overall_et_list=list()
medpu_overall_tt_list=list()
maxpu_overall_wt_list=list()
maxpu_overall_et_list=list()
maxpu_overall_tt_list=list()
medpu_tolerance_list=list()
maxpu_tolerance_list=list()
set_wt_list, set_et_list, set_tt_list= ([] for i in range(3))
medpu_fortask_et_list, medpu_fortask_wt_list, medpu_fortask_tt_list= ([] for i in range(3))
maxpu_fortask_et_list, maxpu_fortask_wt_list, maxpu_fortask_tt_list= ([] for i in range(3))
medpu_set_et_list, medpu_set_wt_list, medpu_set_tt_list= ([] for i in range(3))
maxpu_set_et_list, maxpu_set_wt_list, maxpu_set_tt_list= ([] for i in range(3))
cpuslack_list, cpumem_list, noalign_list= ([] for i in range(3))
flag=100;
xaxis=['x1','x2']
FF_wt_list_g1=list()
BP_wt_list_g1=list()
FF_et_list_g1=list()
BP_et_list_g1=list()

FF_wt_list_g2=list()
BP_wt_list_g2=list()
FF_et_list_g2=list()
BP_et_list_g2=list()

max_pow_ff_list=list()
max_pow_bp_list=list()
perc_pow_ff_list=list()
perc_pow_bp_list=list()

output_path=""

# how watts tolerance is representation in the filename.
wtol_usage_in_filename='pWTol' # default value.

# number of powerclasses that the hosts are categorized into.
num_powerclasses=4 # default value.

# prefix used prior to specifying the Watts Tolerance in the filename.
# for example, blah_blah-blah-25pWTol-blah_blah.blah will have '-' as the prefix prior to 25pWTol specification.
# Note that there has to be a prefix before the WTol specification in the filename.
prefix_before_wtol_in_filename="-" # default value.

# By default plotting the 90th percentile of power consumption.
# If need to plot a different percentile, specify using --power_profile_percentile cmdline arg.
power_profile_percentile=90

# By default power plots include cpu power consumption.
# If dram power consumption is to be included, then set this variable to true.
plot_dram_power=False

# extract Watts Tolerance value.
def extract_wtol_value(s):
    print("EXTRACT_WTOL_VALUE()")
    global prefix_before_wtol_in_filename
    global wtol_usage_in_filename

    regex = prefix_before_wtol_in_filename+"[0-9]*"+wtol_usage_in_filename
    index = re.search(regex, s).start()
    watts_tolerance = s[index+len(prefix_before_wtol_in_filename):(s.index(wtol_usage_in_filename))]
    return watts_tolerance

#Main function
def main():
    print("MAIN()")
    parser=argparse.ArgumentParser(description='Files for computing the average')
    parser.add_argument("--power",help="Graph for Tolerance vs Power",required=False,action='store_true')
    parser.add_argument("--plot_all",help="Graph for Tolerance vs Power",required=False,action='store_true')
    parser.add_argument("--dir",nargs='*',help="Multiple directory paths to compute average",required=False)
    parser.add_argument("--files",nargs='*',help="Multiple file paths to compute average",required=False)
    parser.add_argument("--dir_out",help="Output file to print results",required=False)
    parser.add_argument("--output_file",help="Output file to print results",required=False)
    parser.add_argument("--task",help="Plot data for the given task name",required=False)
    parser.add_argument("--powerclass",help="Plot for power classes and wait time",required=False,action='store_true')
    parser.add_argument("--png", help='Name of PNG file to save in', required=False)
    parser.add_argument("--et", help='Tolerance vs Execution time plot', required=False,action='store_true')
    parser.add_argument("--wt", help='Tolerance vs Wait time plot', required=False,action='store_true')
    parser.add_argument("--tat", help='Tolerance vs Turnaround time plot', required=False,action='store_true')
    parser.add_argument("--profile", help='Options- medmedmedpu or medmedmaxpeakpu', required=False)
    parser.add_argument("--tasks",help="Plot data for given set of tasks",nargs='*',required=False)
    parser.add_argument("--set_output",help="Output file to print results of set of tasks",required=False)
    parser.add_argument("--graph_title", help='Title for the generated graph', required=False)
    parser.add_argument("--tasks_group",help="Plot data for given set of tasks",nargs='*',required=False)
    parser.add_argument("--ff_bp",help="Get data for FF and BP",nargs='*',required=False)
    parser.add_argument("--plot_power_tol_align",help='Power vs Tolerance for all alignments', required=False,action='store_true')
    global power_profile_percentile
    parser.add_argument("--power_profile_percentile",help="Percentile of the peak power consumption to plot", required=False,default=power_profile_percentile)
    parser.add_argument("--output_path",help='Provide path for the html files',required=False)
    global wtol_usage_in_filename
    parser.add_argument("--wtol_usage_in_filename",help="Watts Tolerance representation in filename.", required=False, default=wtol_usage_in_filename)
    global prefix_before_wtol_in_filename
    parser.add_argument("--prefix_before_wtol_in_filename",help="Prefix used prior to specifying the Watts Tolerance in the filename", required=False, default=prefix_before_wtol_in_filename)
    global num_powerclasses
    parser.add_argument("--num_powerclasses", help="Number of power classes", required=False, type=int, default=num_powerclasses)
    global plot_dram_power
    parser.add_argument("--plot_dram_power",help='Include dram power consumption in power plots', required=False, action='store_false')
    args=parser.parse_args()

    global output_path
    output_path=args.output_path
    ff_bp_dirs=args.ff_bp
    group_tasks=args.tasks_group
    graph_title=args.graph_title
    plot_tasks=args.tasks
    power=args.power
    plot_all=args.plot_all
    dir_names=list()
    all_align=args.plot_power_tol_align
    plot_for_task=args.task
    power_plot=args.powerclass
    profile=args.profile
    png_file=args.png
    et_flag=args.et
    tt_flag=args.tat
    wt_flag=args.wt
    plot_task_wt_list=list()
    plot_task_tt_list=list()
    plot_task_et_list=list()
    medmax_list=list()
    runtime_dir_list=list()
    sched_dir_list=list()
    file_list=()
    pcp_dir_list=list()
    ff_bp_dir_names=list()

    # initializing how watts tolerance is represented in electron log filenames.
    # this is used when parsing the files and retrieving the watts tolerance values.
    if args.wtol_usage_in_filename != "":
        wtol_usage_in_filename = args.wtol_usage_in_filename

    # initializing number of powerclasses.
    if args.num_powerclasses > 0:
        num_powerclasses = args.num_powerclasses

    # powerclasses divided into 3 or 4 sets.
    global powerClass
    if num_powerclasses == 3:
        powerClass = powerClass_sets_three
    elif num_powerclasses == 4:
        powerClass = powerClass_sets_four

    # initializing prefix before Watts Tolerance in filename.
    prefix_before_wtol_in_filename = args.prefix_before_wtol_in_filename

    # initializing power_profile_percentile to use when plotting power profile.
    power_profile_percentile = int(args.power_profile_percentile)

	# initializing resource for power plots.
    if args.plot_dram_power:
        plot_dram_power = args.plot_dram_power

    global pcp_docs_list,runtime_docs_list,FF_et_list_g1,BP_et_list_g1,FF_wt_list_g1,BP_wt_list_g1,FF_et_list_g2,BP_et_list_g2,FF_wt_list_g2,BP_wt_list_g2
    global overall_wt_list,overall_et_list,cpumem_list,cpuslack_list
#If FF and BP files given
    if ff_bp_dirs and power:
        global max_pow_ff_list,max_pow_bp_list,perc_pow_ff_list,perc_pow_bp_list
        ff_bp_dir_names=get_ff_bp_files(ff_bp_dirs,ff_bp_dir_names)
        get_metrics(ff_bp_dirs,runtime_dir_list,sched_dir_list,pcp_dir_list)
        average_peak_list,max_peak_list,percentile_peak_list=get_power_stats()
        out=args.dir_out
        write_power_stats(average_peak_list,max_peak_list,percentile_peak_list,"ffbp"+out)
        max_pow_ff_list.append(max_peak_list[0])
        max_pow_bp_list.append(max_peak_list[1])
        perc_pow_ff_list.append(percentile_peak_list[0])
        perc_pow_bp_list.append(percentile_peak_list[1])
        max_pow_ff_list=max_pow_ff_list*5
        max_pow_bp_list=max_pow_bp_list*5
        perc_pow_ff_list=perc_pow_ff_list*5
        perc_pow_bp_list=perc_pow_bp_list*5
        runtime_dir_list[:]=[]
        pcp_dir_list[:]=[]
        sched_dir_list[:]=[]
        runtime_docs_list[:]=[]
        pcp_docs_list[:]=[]
        sched_docs_list[:]=[]
    if ff_bp_dirs and not power:
        out=args.dir_out
        global task_average_map
        ff_bp_dir_names=get_ff_bp_files(ff_bp_dirs,ff_bp_dir_names)
        get_metrics(ff_bp_dirs,runtime_dir_list,sched_dir_list,pcp_dir_list)
        get_overalls(out,"d",runtime_dir_list)
        per_task_cluster_averages()
        get_stats(group_tasks,runtime_dir_list,"group1_ffbp.txt")
        task_list = [x for x in taskList if x not in group_tasks]
        FF_et_list_g1.append(medpu_set_et_list[0])
        BP_et_list_g1.append(medpu_set_et_list[1])
        FF_wt_list_g1.append(medpu_set_wt_list[0])
        BP_wt_list_g1.append(medpu_set_wt_list[1])
        FF_et_list_g1=FF_et_list_g1 * 5
        BP_et_list_g1=BP_et_list_g1 * 5
        FF_wt_list_g1=FF_wt_list_g1 * 5
        BP_wt_list_g1=BP_wt_list_g1 * 5
        medpu_set_et_list[:] = []
        medpu_set_wt_list[:] = []
        get_stats(task_list,runtime_dir_list,"group2_ffbp.txt")
        FF_et_list_g2.append(medpu_set_et_list[0])
        BP_et_list_g2.append(medpu_set_et_list[1])
        FF_wt_list_g2.append(medpu_set_wt_list[0])
        BP_wt_list_g2.append(medpu_set_wt_list[1])
        FF_et_list_g2=FF_et_list_g2 * 5
        BP_et_list_g2=BP_et_list_g2 * 5
        FF_wt_list_g2=FF_wt_list_g2 * 5
        BP_wt_list_g2=BP_wt_list_g2 * 5
        medpu_set_et_list[:] = []
        medpu_set_wt_list[:] = []
        runtime_dir_list[:]=[]
        pcp_dir_list[:]=[]
        sched_dir_list[:]=[]
        runtime_docs_list[:]=[]
        pcp_docs_list[:]=[]
        sched_docs_list[:]=[]
        #sys.exit()
#Using the --dir option

    if args.dir:
        global task_average_map
        out=args.dir_out
        set_out=args.set_output
        dir_names=get_files_from_dir(args.dir,dir_names,group_tasks)
        if power and all_align:
            power_trace=[]
            tolerance_values=get_folders_for_align(args.dir)
            get_metrics(cpumem_list,runtime_dir_list,sched_dir_list,pcp_dir_list)
            average_peak_list,max_peak_list,percentile_peak_list=get_power_stats()
            make_align_power_trace(percentile_peak_list,"cpu-mem",power_trace,tolerance_values)
            pcp_dir_list=[]
            pcp_docs_list=[]
            percentile_peak_list=[]
            get_metrics(cpuslack_list,runtime_dir_list,sched_dir_list,pcp_dir_list)
            average_peak_list,max_peak_list,percentile_peak_list=get_power_stats()
            make_align_power_trace(percentile_peak_list,"cpu-power",power_trace,tolerance_values)
            pcp_dir_list=[]
            pcp_docs_list=[]
            percentile_peak_list=[]
            get_metrics(noalign_list,runtime_dir_list,sched_dir_list,pcp_dir_list)
            average_peak_list,max_peak_list,percentile_peak_list=get_power_stats()
            make_align_power_trace(percentile_peak_list,"no-align",power_trace,tolerance_values)
            plot_power_tolerance_align(power_trace,graph_title)
            sys.exit()
        if all_align and not power:
            file=open(out,"w")
            file.write("Overall Stats"+"\n"+"======================="+"\n")
            file.write("Average Wait-Time"+"\n")
            file.close()
            align_et_data_trace=[]
            align_wt_data_trace=[]
            tolerance_values=get_folders_for_align(args.dir)
            get_metrics(cpumem_list,runtime_dir_list,sched_dir_list,pcp_dir_list)
            get_overalls(out,"d",runtime_dir_list)
            make_alignment_trace(overall_wt_list,overall_et_list,"cpu-mem",tolerance_values,align_et_data_trace,align_wt_data_trace)

            runtime_dir_list=[]
            cpumem_list=[]
            overall_wt_list=[]
            overall_et_list=[]
            runtime_docs_list=[]
            get_metrics(cpuslack_list,runtime_dir_list,sched_dir_list,pcp_dir_list)
            get_overalls(out,"d",runtime_dir_list)
            make_alignment_trace(overall_wt_list,overall_et_list,"cpu-power",tolerance_values,align_et_data_trace,align_wt_data_trace)

            runtime_dir_list=[]
            cpuslack_list=[]
            overall_wt_list=[]
            overall_et_list=[]
            runtime_docs_list=[]
            get_metrics(noalign_list,runtime_dir_list,sched_dir_list,pcp_dir_list)
            get_overalls(out,"d",runtime_dir_list)
            make_alignment_trace(overall_wt_list,overall_et_list,"no-align",tolerance_values,align_et_data_trace,align_wt_data_trace)
            plot_alignment(align_et_data_trace,align_wt_data_trace,graph_title);

            sys.exit()
        get_metrics(dir_names,runtime_dir_list,sched_dir_list,pcp_dir_list)
        file=open(out,"w")
        file.write("Overall Stats"+"\n"+"======================="+"\n")
        file.write("Average Wait-Time"+"\n")
        file.close()
        #To plot only Power vs Tolerance
        if power and not plot_all:
            average_peak_list,max_peak_list,percentile_peak_list=get_power_stats()
            out=args.dir_out
            write_power_stats(average_peak_list,max_peak_list,percentile_peak_list,out)
            plot_power_tolerance(average_peak_list,max_peak_list,percentile_peak_list,graph_title,group_tasks)
        if not plot_tasks and not power and group_tasks:
            get_overalls(out,"d",runtime_dir_list)
            per_task_cluster_averages()
            if ff_bp_dirs:
                for taskN in taskList:
                    temp_list=task_average_map[taskN]
                    temp_list=temp_list[2:]
                    task_average_map[taskN]=temp_list
            task_list = [x for x in taskList if x not in group_tasks]
            get_stats(group_tasks,runtime_dir_list,"group1.txt")
            et_data_trace=[]
            wt_data_trace=[]
            if ff_bp_dirs and len(group_tasks)==2:
                #tasks_title=",".join(group_tasks)
                tasks_title="High Impact Tasks (HI)"
            else:
                titles=graph_title.split('-')
                tasks_title=titles[0]
            make_group_trace(tasks_title,et_flag,wt_flag,et_data_trace,wt_data_trace)
            medpu_set_et_list[:] = []
            medpu_set_wt_list[:] = []
            maxpu_set_et_list[:] = []
            maxpu_set_wt_list[:] = []
            get_stats(task_list,runtime_dir_list,"group2.txt")
            if not ff_bp_dirs and len(group_tasks)==2:
                tasks_title="Non-Critical Tasks"
            else:
                titles=graph_title.split('-')
                # CHANGING THE LEGEND as and when required.
                tasks_title=titles[1]
                # tasks_title = "Low Impact Tasks (LI)"

            make_group_trace(tasks_title,et_flag,wt_flag,et_data_trace,wt_data_trace)
            if ff_bp_dirs and len(group_tasks)!=2:
                plot_ff_bp_group_trace(et_data_trace,wt_data_trace,graph_title)
            elif ff_bp_dirs and len(group_tasks)==2:
                plot_group_trace(et_data_trace,wt_data_trace,graph_title)
        # To plot for all Tasks
        elif not plot_tasks and not power:
            get_overalls(out,"d",runtime_dir_list)
            per_task_cluster_averages()
            perTask_perClass_avg_exec_time()
            average_peak_list,max_peak_list,percentile_peak_list=get_power_stats()
            write_results(out,plot_for_task,plot_task_tt_list,plot_task_wt_list,plot_task_et_list,runtime_dir_list,sched_dir_list,"d",plot_tasks)
            plot_tolerance_metrics(plot_for_task,et_flag,wt_flag,tt_flag,profile,plot_tasks,plot_all,
            average_peak_list,max_peak_list,percentile_peak_list,graph_title)
        # To plot for Set of Tasks
        elif plot_tasks and not power:
            get_overalls(out,"d",runtime_dir_list)
            per_task_cluster_averages()
            perTask_perClass_avg_exec_time()
            average_peak_list,max_peak_list,percentile_peak_list=get_power_stats()
            write_results(out,plot_for_task,plot_task_tt_list,plot_task_wt_list,plot_task_et_list,runtime_dir_list,sched_dir_list,"d",plot_tasks)
            get_stats(plot_tasks,runtime_dir_list,set_out)
            plot_tolerance_metrics(plot_for_task,et_flag,wt_flag,tt_flag,profile,plot_tasks,plot_all,
            average_peak_list,max_peak_list,percentile_peak_list,graph_title)

    #Using the file option
    if args.files:
        out=args.output_file
        file=open(out,"w")
        file.write("Overall Stats"+"\n"+"======================="+"\n")
        file.write("Average Wait-Time"+"\n")
        file.close()
        global file_names
        for f in args.files:
            key,value=f.split('=')
            file_names.append(key)

        #Read multiple files
        i=0
        while i<len(args.files):
            key,value=args.files[i].split('=')
            metrics_df=pd.read_csv(value,skiprows=1,sep=',',names=headers)
            runtime_docs_list.append(metrics_df)
            i=i+2

        i=1
        while i<len(args.files):
            key,value=args.files[i].split('=')
            schedFrame_df=pd.read_csv(value,sep=" ",names=schedTraceHeaders)
            sched_docs_list.append(schedFrame_df)
            i=i+2
        get_overalls(out,"f",runtime_dir_list)
        per_task_cluster_averages()
        perTask_perClass_avg_exec_time()
        write_results(out,plot_for_task,plot_task_tt_list,plot_task_wt_list,plot_task_et_list,runtime_dir_list,sched_dir_list,"f",plot_tasks)
        plot_metrics(plot_task_tt_list,plot_task_wt_list,plot_task_et_list,plot_for_task,power_plot,png_file)

def get_stats(plot_tasks,runtime_dir_list,set_out):
    print("GET_STATS()")

    global task_average_map
    #For a set of tasks get metrics
    temp_list_wt=[]
    temp_list_et=[]
    temp_list_tt=[]
    for p in plot_tasks:
        temp_list_wt.append(task_average_map[p])
        temp_list_et.append(task_average_exec_map[p])
        temp_list_tt.append(task_average_turn_map[p])
    #Add metrics and calculate the averages
    length=len(plot_tasks)
    sumlist_wt=[sum(i) for i in zip(*temp_list_wt)]
    set_wt_list = [round((x / length),3) for x in sumlist_wt]
    sumlist_et=[sum(i) for i in zip(*temp_list_et)]
    set_et_list = [round((x / length),3) for x in sumlist_et]
    sumlist_tt=[sum(i) for i in zip(*temp_list_tt)]
    set_tt_list = [round((x / length),3) for x in sumlist_tt]
    file=open(set_out,"w")
    file.write("Stats for Set of Tasks"+"\n"+"======================="+"\n")
    j=0
    #Write original metrics as well as the new averages to a output file
    while j<len(plot_tasks):
        file.write("\n"+"Task Name: "+plot_tasks[j]+"\n")
        file.write("Average Wait-Time"+"\n")
        k=0
        while k<len(task_average_map[plot_tasks[j]]):
            file.write(runtime_dir_list[k]+": "+str(task_average_map[plot_tasks[j]][k])+"\n")
            k=k+1
        file.write("Average Execution-Time"+"\n")
        k=0
        while k<len(task_average_exec_map[plot_tasks[j]]):
            file.write(runtime_dir_list[k]+": "+str(task_average_exec_map[plot_tasks[j]][k])+"\n")
            k=k+1
        file.write("Average Turnaround-Time"+"\n")
        k=0
        while k<len(task_average_turn_map[plot_tasks[j]]):
            file.write(runtime_dir_list[k]+": "+str(task_average_turn_map[plot_tasks[j]][k])+"\n")
            k=k+1
        j=j+1
    file.write("\n"+"Averages of Metrics"+"\n"+"====================="+"\n")
    file.write("Average Wait-Time"+"\n")
    j=0
    while j<len(set_wt_list):
        file.write(runtime_dir_list[j]+": "+str(set_wt_list[j])+"\n")
        j=j+1
    file.write("Average Execution-Time"+"\n")
    j=0
    while j<len(set_et_list):
        file.write(runtime_dir_list[j]+": "+str(set_et_list[j])+"\n")
        j=j+1
    file.write("Average Turnaround-Time"+"\n")
    j=0
    while j<len(set_tt_list):
        file.write(runtime_dir_list[j]+": "+str(set_tt_list[j])+"\n")
        j=j+1

    file.close()
    # Separate the set of tasks into two profiling methods and these lists are further used for plotting graph
    i=0

    while i<len(runtime_dir_list):
        if "medmedmedpu" in runtime_dir_list[i]:
            medpu_set_wt_list.append(set_wt_list[i])
            medpu_set_et_list.append(set_et_list[i])
            medpu_set_tt_list.append(set_tt_list[i])
        elif "medmedmaxpeakpu" in runtime_dir_list[i]:
            maxpu_set_wt_list.append(set_wt_list[i])
            maxpu_set_et_list.append(set_et_list[i])
            maxpu_set_tt_list.append(set_tt_list[i])
        i=i+1

#Function to plot Power vs Tolerance for all 3 alignments
def make_align_power_trace(percentile_peak_list,name,power_trace,tolerance_values):
    print("MAKE_ALIGN_POWER_TRACE()")
    global flag;
    trace=go.Scatter(x=tolerance_values,y=percentile_peak_list,name=name,line=dict(width=3),marker=dict(symbol=flag,size=15,line=dict(width=2)))
    power_trace.append(trace)
    flag=flag+1
#Function to make 3 traces for the three alignment methods
def make_alignment_trace(overall_wt_list,overall_et_list,name,tolerance_values,align_et_data_trace,align_wt_data_trace):
    print("MAKE_ALIGNMENT_TRACE()")
    global flag;
    trace_wt=go.Scatter(x=tolerance_values,y=overall_wt_list,name=name,line=dict(width=3),marker=dict(symbol=flag,size=15))
    trace_et=go.Scatter(x=tolerance_values,y=overall_et_list,name=name,line=dict(width=3),marker=dict(symbol=flag,size=15))
    align_et_data_trace.append(trace_et)
    align_wt_data_trace.append(trace_wt)
    flag=flag+1

#Function to make 2 traces for two groups
def make_group_trace(name,et_flag,wt_flag,et_data_trace,wt_data_trace):
    print("MAKE_GROUP_TRACE()")
    global flag;
    list_to_plot=list()
    global medpu_tolerance_list
    if maxpu_tolerance_list:
        medpu_tolerance_list=maxpu_tolerance_list
    if et_flag:
        if medpu_set_et_list:
            list_to_plot=medpu_set_et_list
        else:
            list_to_plot=maxpu_set_et_list
        trace=go.Scatter(x=medpu_tolerance_list,y=list_to_plot,name=name,line=dict(width=5),
        marker=dict(symbol=flag,size=20,line=dict(width=3)))
        et_data_trace.append(trace)
    if wt_flag:
        if medpu_set_wt_list:
            list_to_plot=medpu_set_wt_list
        else:
            list_to_plot=maxpu_set_wt_list
        trace=go.Scatter(x=medpu_tolerance_list,y=list_to_plot,name=name,line=dict(width=5),
        marker=dict(symbol=flag,size=15,line=dict(width=3)))
        wt_data_trace.append(trace)
    flag=flag+1

def get_folders_for_align(dir):
    print("GET_FOLDERS_FOR_ALIGN()")
    global cpumem_list,cpuslack_list,noalign_list
    dir_tolerance_list=[]
    for d in dir:
        key,value=d.split('=')
        if "CpuMem" in value:
            cpumem_list.append(value)
            tolerance_val = extract_wtol_value(value)
            dir_tolerance_list.append(int(tolerance_val))
    cpumem_list= [x for _,x in sorted(zip(dir_tolerance_list,cpumem_list))]
    if cpumem_list:
        dir_tolerance_list.sort()
        tolerance_values=dir_tolerance_list
    dir_tolerance_list=[]
    for d in dir:
        key,value=d.split('=')
        if "CpuPower" in value:
            cpuslack_list.append(value)
            tolerance_val = extract_wtol_value(value)
            dir_tolerance_list.append(int(tolerance_val))
    cpuslack_list= [x for _,x in sorted(zip(dir_tolerance_list,cpuslack_list))]
    if cpuslack_list:
        dir_tolerance_list.sort()
        tolerance_values=dir_tolerance_list

    dir_tolerance_list=[]
    for d in dir:
        key,value=d.split('=')
        if "noAlign" in value:
            noalign_list.append(value)
            tolerance_val = extract_wtol_value(value)
            dir_tolerance_list.append(int(tolerance_val))
    noalign_list= [x for _,x in sorted(zip(dir_tolerance_list,noalign_list))]
    if noalign_list:
        dir_tolerance_list.sort()
        tolerance_values=dir_tolerance_list

    return tolerance_values

#For FF and BP files
def get_ff_bp_files(dir,ff_bp_dir_names):
    print("GET_FF_BP_FILES()")
    for d in dir:
        ff_bp_dir_names.append(d)
    return ff_bp_dir_names

# Get files from the directory and getting the tolerance values
def get_files_from_dir(dir,dir_names,group_tasks):
    print("GET_FILES_FROM_DIR()")
    dir_tolerance_list=[]
    for d in dir:
        key,value=d.split('=')
        dir_names.append(value)
        tolerance_val = extract_wtol_value(value)
        dir_tolerance_list.append(int(tolerance_val))
    dir_names = [x for _,x in sorted(zip(dir_tolerance_list,dir_names))]

#Separate Tolerance lists for medmedmedpu and medmedmaxpeakpu
    for value in dir_names:
        tolerance_val = extract_wtol_value(value)
        if "medmedmedpu" in value:
            medpu_tolerance_list.append(int(tolerance_val))
        if "medmedmaxpeakpu" in value:
            maxpu_tolerance_list.append(int(tolerance_val))
    return dir_names

# Get entries from schedTrace.log , runtimeMetrics.log and .pcplog
def get_metrics(dir_list,runtime_dir_list,sched_dir_list,pcp_dir_list):
    print("GET_METRICS()")
    for ele in dir_list:
        files=listdir(ele)
        for file in files:
            if 'schedTrace.log' in file:
                sched_dir_list.append(file)
                schedFrame_df=pd.read_csv(ele+""+file,sep=" ",names=schedTraceHeaders)
                sched_docs_list.append(schedFrame_df)
            if 'runtimeMetrics.log' in file:
                runtime_dir_list.append(file)
                metrics_df=pd.read_csv(ele+""+file,skiprows=1,sep=',',names=headers)
                runtime_docs_list.append(metrics_df)
            if '.pcplog' in file:
                pcp_dir_list.append(file)
                pcpDF=pd.read_csv(ele+""+file)
                pcpDF=cleanup(pcpDF)
                pcp_docs_list.append(pcpDF)

def totalPowerUsage(pcpDF):
    print("TOTALPOWERUSAGE()")
    global plot_dram_power

    """
    Calculates total power by summing power entries (CPU and/or DRAM) for all nodes in cluster
    Adds a column `total-power` to the DataFrame
    """
    term = ':perfevent.hwcounters.rapl__RAPL_ENERGY_PKG'

	# Generalizing the prefix to include dram power consumption as well.
	# dram power readings -> perfevent.hwcounters.rapl__RAPL_ENERGY_DRAM.
	# 'perfevent.hwcounters.rapl__RAPL_ENERGY' is the common prefix for both cpu and dram readings.

    if plot_dram_power:
        term = ':perfevent.hwcounters.rapl__RAPL_ENERGY'

    normalize = 2**32

    pcpDF = pcpDF.filter(regex=term)
    pcpDF['total-power'] = pcpDF.sum(axis=1) / normalize

    return pcpDF

def cleanup(df):
    print("CLEANUP()")
    df = df[1:]
    return df

# Get average peak power, max peak power, 90th percentile power from the pcplog
def get_power_stats():
    global power_profile_percentile
    print("GET_POWER_STATS()")
    average_peak_list=[]
    max_peak_list=[]
    percentile_peak_list=[]
    for pcpDF in pcp_docs_list:
            pcpDF=totalPowerUsage(pcpDF)
            average_peak_list.append(np.average(pcpDF['total-power']))
            max_peak_list.append(np.max(pcpDF['total-power']))
            percentile_peak_list.append(np.percentile(pcpDF['total-power'],power_profile_percentile))
    return average_peak_list,max_peak_list,percentile_peak_list


def get_overalls(out,mode,runtime_dir_list):
    print("GET_OVERALLS()")
    #Calculate average-wait time for all runs
    i=0
    j=0
    while i<len(runtime_docs_list):
        if mode=="f":
            key=file_names[j]
        elif mode=="d":
            key=runtime_dir_list[i]
        allTasks_avg_wait_time(runtime_docs_list[i],out,key)
        i=i+1
        j=j+2

    file=open(out,"a")
    file.write("Average Execution-Time"+"\n")
    file.close()
    #Calculate average-execution time for all runs
    i=0
    j=0
    while i<len(runtime_docs_list):
        if mode=="f":
            key=file_names[j]
        elif mode=="d":
            key=runtime_dir_list[i]
        allTasks_execution_time(runtime_docs_list[i],out,key)
        i=i+1
        j=j+2
    #Calculate Turnaround Time
    file=open(out,"a")
    file.write("Average Turnaround-Time"+"\n")
    file.close()
    i=0
    j=0
    while i<len(runtime_docs_list):
        if mode=="f":
            key=file_names[j]
        elif mode=="d":
            key=runtime_dir_list[i]
        allTasks_avg_turn_time(runtime_docs_list[i],out,key)
        i=i+1
        j=j+2

    file=open(out,"a")
    file.write("\n")
    file.close()


# Function to plot different metrics according to the flags provided
def plot_tolerance_metrics(plot_for_task,et_flag,wt_flag,tt_flag,profile,plot_tasks,plot_all,average_peak_list,
max_peak_list,percentile_peak_list,graph_title):
    print("PLOT_TOLERANCE_METRICS()")
    # Plot for a single task depending on the flags provided for (ET,WT,TAT)
    if  plot_for_task:
       if plot_for_task and et_flag:
           plot_tolerance(medpu_fortask_et_list,maxpu_fortask_et_list,graph_title,"task-et.html","Execution-Time (seconds)")
       if plot_for_task and wt_flag:
           plot_tolerance(medpu_fortask_wt_list,maxpu_fortask_wt_list,graph_title,"task-wt.html","Wait-Time (seconds)")
       if plot_for_task and tt_flag:
           plot_tolerance(medpu_fortask_tt_list,maxpu_fortask_tt_list,graph_title,"task-tat.html","Turnaround-Time (seconds)")
    # Plot for a specific profile provided from the command line for a Set of Tasks
    elif profile and plot_tasks and not plot_all:
        if profile=='medmedmedpu':
            if et_flag:
                plot_profile_and_metric(medpu_set_et_list,graph_title+" Average Execution Time","Average Execution Time",graph_title+"-ExecTime.html")
            if wt_flag:
                plot_profile_and_metric(medpu_set_wt_list,graph_title+" Average Wait Time","Average Wait Time",graph_title+"-WaitTime.html")
            plot_metrics_for_profile(medpu_set_wt_list,medpu_set_et_list,medpu_set_tt_list,graph_title)
        if profile=='medmedmaxpeakpu':
            if et_flag:
                plot_profile_and_metric(maxpu_set_et_list,graph_title,"Average Execution Time",graph_title+"-ExecTime.html")
            if wt_flag:
                plot_profile_and_metric(maxpu_set_wt_list,graph_title,"Average Wait Time",graph_title+"-WaitTime.html")
            plot_metrics_for_profile(maxpu_set_wt_list,maxpu_set_et_list,maxpu_set_tt_list,graph_title)
    # Plot for a specific profile provided from the command line for All Tasks
    elif profile and not plot_all:
        if profile=='medmedmedpu':
            if et_flag:
                plot_profile_and_metric(medpu_overall_et_list,graph_title,"Average Execution Time",graph_title+"-ExecTime.html")
            if wt_flag:
                plot_profile_and_metric(medpu_overall_wt_list,graph_title,"Average Wait Time",graph_title+"-WaitTime.html")
            plot_metrics_for_profile(medpu_overall_wt_list,medpu_overall_et_list,medpu_overall_tt_list,graph_title)
        if profile=='medmedmaxpeakpu':
            if et_flag:
                plot_profile_and_metric(maxpu_overall_et_list,graph_title+" Average Execution Time","Average Execution Time",graph_title+"-ExecTime.html")
            if wt_flag:
                plot_profile_and_metric(maxpu_overall_wt_list,graph_title+" Average Wait Time","Average Wait Time",graph_title+"-WaitTime.html")
            plot_metrics_for_profile(maxpu_overall_wt_list,maxpu_overall_et_list,maxpu_overall_tt_list,graph_title)
    # Plot for both profiles for a Set of Tasks and for the provided metrics
    if plot_tasks and not plot_all and not profile:
        if et_flag:
            plot_tolerance(medpu_set_et_list,maxpu_set_et_list,graph_title+" Execution-Time",graph_title+"-et.html","Execution-Time (seconds)")
        if wt_flag:
            plot_tolerance(medpu_set_wt_list,maxpu_set_wt_list,graph_title+" Wait-Time",graph_title+"-wt.html","Wait-Time (seconds)")
        if tt_flag:
            plot_tolerance(medpu_set_tt_list,maxpu_set_tt_list,graph_title+" Turnaround-Time",graph_title+"-tat.html","Turnaround-Time (seconds)")
    # Plot for both profiles of All Tasks and for the provided metrics
    elif not plot_tasks and not plot_all and not profile:
        if et_flag:
            plot_tolerance(medpu_overall_et_list,maxpu_overall_et_list,graph_title+" Execution-Time",graph_title+"-et.html","Time (seconds)")
        if wt_flag:
            plot_tolerance(medpu_overall_wt_list,maxpu_overall_wt_list,graph_title+" Wait-Time",graph_title+"-wt.html","Time (seconds)")
        if tt_flag:
            plot_tolerance(medpu_overall_tt_list,maxpu_overall_tt_list,graph_title+" Turnaround-Time","overall-tt.html","Time (seconds)")
    # Plot for both profiles for a Set of Tasks along with the Power on another y axis for the provided metrics
    if not plot_tasks and plot_all:
        if et_flag:
            plot_time_power_tolerance(average_peak_list,max_peak_list,percentile_peak_list,medpu_overall_et_list,maxpu_overall_et_list
            ,"allTasks-execution-time-power-tolerance.html","Execution-Time (seconds)", graph_title+" Execution-Time")
        if wt_flag:
            plot_time_power_tolerance(average_peak_list,max_peak_list,percentile_peak_list,medpu_overall_wt_list,maxpu_overall_wt_list
            ,"allTasks-wait-time-power-tolerance.html","Wait-Time (seconds)",graph_title+" Wait-Time")
    # Plot for both profiles for All Tasks along with the Power on another y axis for the provided metrics
    if plot_tasks and plot_all:
        if et_flag:
            plot_time_power_tolerance(average_peak_list,max_peak_list,percentile_peak_list,medpu_set_et_list,
            maxpu_overall_et_list,graph_title+"-exec-time-power-tolerance.html","Execution-Time (seconds)",graph_title+" Execution-Time")
        if wt_flag:
            plot_time_power_tolerance(average_peak_list,max_peak_list,percentile_peak_list,medpu_set_wt_list,
            maxpu_overall_wt_list,graph_title+"-wait-time-power-tolerance.html","Wait-Time (seconds)",graph_title+ " Wait-Time")
#Function to plot Power vs Tolerance for alignments
def plot_power_tolerance_align(power_trace,graph_title):
    print("PLOT_POWER_TOLERANCE_ALIGN()")
    layout=go.Layout(title=graph_title,yaxis=dict(title="Power (Watts)",gridwidth=1,gridcolor='#000000',titlefont=dict(size=26),tickfont=dict(size=26)),
    xaxis=dict(title='Power_Tolerance (%)',gridwidth=1,gridcolor='#000000',dtick=10,tickwidth=4,titlefont=dict(size=26),tickfont=dict(size=26))
    ,legend=dict(x=0.40
    ,traceorder='normal',font=dict(family='sans-serif',size=20,color='#000'),bgcolor='#E2E2E2',
    bordercolor='#FFFFFF',borderwidth=1.3))
    fig=go.Figure(data=power_trace,layout=layout)
    plot(fig,auto_open=False,filename=graph_title+"-powerVtolerance",
    image_filename=graph_title+"-powerVtolerance",image='png',image_width=850,image_height=600)
#Function to plot Alignments
def plot_alignment(align_et_data_trace,align_wt_data_trace,graph_title):
    print("PLOT_ALIGNMENT()")

    layout=go.Layout(title=graph_title,yaxis=dict(title="Execution Time (seconds)",gridwidth=1,gridcolor='#000000'),
    xaxis=dict(title='Power_Tolerance (%)',gridwidth=1,gridcolor='#000000',dtick=10,tickwidth=4),legend=dict(x=0.45
    ,traceorder='normal',font=dict(family='sans-serif',size=13,color='#000'),bgcolor='#E2E2E2',
    bordercolor='#FFFFFF',borderwidth=1))
    fig=go.Figure(data=align_et_data_trace,layout=layout)
    plot(fig,auto_open=False,filename=graph_title+"-ExecutionTime",image_filename=graph_title+"-ExecutionTime",image='png')

    layout1=go.Layout(title=graph_title,yaxis=dict(title="Wait Time (seconds)",gridwidth=1,gridcolor='#000000'),
    xaxis=dict(title='Power_Tolerance (%)',gridwidth=1,gridcolor='#000000',dtick=10,tickwidth=4),legend=dict(x=0.45
    ,traceorder='normal',font=dict(family='sans-serif',size=13,color='#000'),bgcolor='#E2E2E2',
    bordercolor='#FFFFFF',borderwidth=1))
    fig1=go.Figure(data=align_wt_data_trace,layout=layout1)
    plot(fig1,auto_open=False,filename=graph_title+"-WaitTime",image_filename=graph_title+"-WaitTime",image='png')

#Function to plot data of two groups
def plot_group_trace(et_data_trace,wt_data_trace,graph_title):
    print("PLOT_GROUP_TRACE()")

    label_list=graph_title.split('-')
    label1=label_list[0]
    label2=label_list[1]
    if et_data_trace:
        FF_label1="FF-"+label1+" "+str(FF_et_list_g1[0])+"s"
        BP_label1="BP-"+label1+" "+str(BP_et_list_g1[0])+"s"
        FF_label2="FF-"+label2+" "+str(FF_et_list_g2[0])+"s"
        BP_label2="BP-"+label2+" "+str(BP_et_list_g2[0])+"s"
        trace1=go.Scatter(x=medpu_tolerance_list,y=FF_et_list_g1,name="FF",
        line=dict(dash = 'dash',width=4,color='#000000'),marker=dict(opacity=0),showlegend=False)

        trace2=go.Scatter(x=medpu_tolerance_list,y=BP_et_list_g1,name="BP"
        ,line=dict(dash = 'dot',width=4,color='#000000'),marker=dict(opacity=0)
        ,showlegend=False)

        trace3=go.Scatter(x=medpu_tolerance_list,y=FF_et_list_g2,name="FF"
        ,line=dict(dash = 'dash',width=4,color='#000000'),marker=dict(opacity=0)
        ,showlegend=False)

        trace4=go.Scatter(x=medpu_tolerance_list,y=BP_et_list_g2,name="BP"
        ,line=dict(dash = 'dot',width=4,color='#000000'),marker=dict(opacity=0)
        ,showlegend=False)

        et_data_trace.append(trace1)
        et_data_trace.append(trace2)
        et_data_trace.append(trace3)
        et_data_trace.append(trace4)

        # CHANGE AX AND AY FOR LABELS IF YOU WANT TO REPOSITION THEM.
        # CURRENTLY CHANGED y VALUE TO POINT TO [FF|BP]_et_list_g[1|2][0] FOR THE UPDATED RUNS USED FOR IPDPS 2020 SUBMISSION.
        # CHANGE range[...] VALUE IF ALTERING THE SCALE.
        # CHANGE THE y VALUE FOR 0%WTol and 100%WTol INDICATORS TO CORRESPOND TO THE UPDATED VALUES.
        layout=go.Layout(annotations=[
        dict(x=25,y=FF_et_list_g2[0],xref='x',yref='y',text=FF_label2,
        showarrow=True,font=dict(size=24,color="#000",family='Arial'),arrowhead=1,ax=0,ay=-80,arrowside="start",arrowwidth=2),
        dict(x=75,y=BP_et_list_g2[0],xref='x',yref='y',text=BP_label2,
        showarrow=True,font=dict(size=24,color="#000",family='Arial'),arrowhead=1,ax=0,ay=-30,arrowside="start",arrowwidth=2),
        dict(x=20,y=FF_et_list_g1[0],xref='x',yref='y',text=FF_label1,
        showarrow=True,font=dict(size=24,color="#000",family='Arial'),arrowhead=1,ax=0,ay=20,arrowside="start",arrowwidth=2),
        dict(x=60,y=BP_et_list_g1[0],xref='x',yref='y',text=BP_label1,
        showarrow=True,font=dict(size=24,color="#000",family='Arial'),arrowhead=1,ax=0,ay=-60,arrowside="start",arrowwidth=2),
        dict(x=0,y=934.99,xref='x',yref='y',text="100% WTol",
        showarrow=True,font=dict(size=24,color="#000",family='Arial'),arrowhead=7,ax=0,ay=40),
        dict(x=0,y=40.152,xref='x',yref='y',text="0% WTol",
        showarrow=True,font=dict(size=24,color="#000",family='Arial'),arrowhead=7,ax=0,ay=-50),
        dict(x=100,y=641.36,xref='x',yref='y',text="0% WTol",
        showarrow=True,font=dict(size=24,color="#000",family='Arial'),arrowhead=7,ax=0,ay=50),
        dict(x=100,y=72.884,xref='x',yref='y',text="100% WTol",
        showarrow=True,font=dict(size=24,color="#000",family='Arial'),arrowhead=7,ax=0,ay=40)]
        ,title="",
        yaxis=dict(title="Average Execution Time (seconds)",gridwidth=3,titlefont=dict(size=24,family="Arial",color="#000000")
        ,tickfont=dict(size=24,family='Arial',color="#000000"),automargin=True,range=[0,1100]),
        xaxis=dict(title='Power_Tolerance (%)',gridwidth=3,dtick=10,
        tickwidth=4,ticktext=["0/100H","25/75H","75/25H","100/0H"],tickvals=medpu_tolerance_list,
        titlefont=dict(size=24,family="Arial",color="#000000")
        ,tickfont=dict(size=26,family='Arial',color="#000000"),automargin=True)
        ,legend=dict(x=0.45,y=0.8
        ,traceorder='normal',font=dict(family='sans-serif',size=20,color='#000'),bgcolor='#E2E2E2',
        bordercolor='#FFFFFF',borderwidth=1))
        fig=go.Figure(data=et_data_trace,layout=layout)
        if output_path:
            plot(fig,auto_open=False,filename=output_path+graph_title+"-ExecutionTime",image_filename=graph_title+"-ExecutionTime",image='png')
        else:
            plot(fig,auto_open=False,filename=graph_title+"-ExecutionTime",image_filename=graph_title+"-ExecutionTime",image='png')
    if wt_data_trace:

        FF_label1="FF-"+label1+" "+str(FF_wt_list_g1[0])+"s"
        BP_label1="BP-"+label1+" "+str(BP_wt_list_g1[0])+"s"
        FF_label2="FF-"+label2+" "+str(FF_wt_list_g2[0])+"s"
        BP_label2="BP-"+label2+" "+str(BP_wt_list_g2[0])+"s"

        trace1=go.Scatter(x=medpu_tolerance_list,y=FF_wt_list_g1,name="FF",line=dict(dash = 'dash',width=4,color='#000000'),marker=dict(opacity=0)
        ,showlegend=False)
        trace2=go.Scatter(x=medpu_tolerance_list,y=BP_wt_list_g1,name="BP",line=dict(dash = 'dot',width=4,color='#000000'),marker=dict(opacity=0)
        ,showlegend=False)
        trace3=go.Scatter(x=medpu_tolerance_list,y=FF_wt_list_g2,name="FF",line=dict(dash = 'dash',width=4,color='#000000'),marker=dict(opacity=0)
        ,showlegend=False)
        trace4=go.Scatter(x=medpu_tolerance_list,y=BP_wt_list_g2,name="BP",line=dict(dash = 'dot',width=4,color='#000000'),marker=dict(opacity=0)
        ,showlegend=False)

        wt_data_trace.append(trace1)
        wt_data_trace.append(trace2)
        wt_data_trace.append(trace3)
        wt_data_trace.append(trace4)

        # CHANGE AX AND AY FOR LABELS IF YOU WANT TO REPOSITION THEM.
        # CURRENTLY CHANGED y VALUE TO POINT TO [FF|BP]_wt_list_g[1|2][0] FOR THE UPDATED RUNS USED FOR IPDPS 2020 SUBMISSION.
        # CHANGE range[...] VALUE IF ALTERING THE SCALE.
        # CHANGE THE y VALUE FOR 0%WTol and 100%WTol INDICATORS TO CORRESPOND TO THE UPDATED VALUES.
        layout=go.Layout(annotations=[
        dict(x=85,y=FF_wt_list_g2[0],xref='x',yref='y',text=FF_label2,
        showarrow=True,font=dict(size=24,color="#000",family='Arial'),arrowhead=1,ax=0,ay=-35,arrowside="start",arrowwidth=2),
        dict(x=25,y=BP_wt_list_g2[0],xref='x',yref='y',text=BP_label2,
        showarrow=True,font=dict(size=24,color="#000",family='Arial'),arrowhead=1,ax=0,ay=20,arrowside="start",arrowwidth=2),
        dict(x=20,y=FF_wt_list_g1[0],xref='x',yref='y',text=FF_label1,
        showarrow=True,font=dict(size=24,color="#000",family='Arial'),arrowhead=1,ax=0,ay=-12,arrowside="start",arrowwidth=2),
        dict(x=20,y=BP_wt_list_g1[0],xref='x',yref='y',text=BP_label1,
        showarrow=True,font=dict(size=24,color="#000",family='Arial'),arrowhead=1,ax=0,ay=-20,arrowside="start",arrowwidth=2),
        dict(x=0,y=592.381,xref='x',yref='y',text="0% WTol",
        showarrow=True,font=dict(color="#000",size=24,family='Arial'),arrowhead=7,ax=0,ay=-30),
        dict(x=0,y=160.36,xref='x',yref='y',text="100% WTol",
        showarrow=True,font=dict(color="#000",size=24,family='Arial'),arrowhead=7,ax=0,ay=-30),
        dict(x=100,y=585.698,xref='x',yref='y',text="100% WTol",
        showarrow=True,font=dict(color="#000",size=24,family='Arial'),arrowhead=7,ax=0,ay=30),
        dict(x=100,y=1528.11,xref='x',yref='y',text="0% WTol",
        showarrow=True,font=dict(color="#000",size=24,family='Arial'),arrowhead=7,ax=0,ay=-30)]
        ,title="",yaxis=dict(title="Average Wait Time (seconds)",gridwidth=3,tickfont=dict(size=24,family="Arial",color="#000000")
        ,titlefont=dict(size=24,family="Arial",color="#000000"),automargin=True),
        xaxis=dict(title='Power_Tolerance (%)',gridwidth=3,
        dtick=10,tickwidth=4,ticktext=["0/100H","25/75H","75/25H","100/0H"],tickvals=medpu_tolerance_list,
        tickfont=dict(size=24,family="Arial",color="#000000"),titlefont=dict(size=24,family="Arial",color="#000000"),automargin=True),
        legend=dict(x=0.35,
        y=0.75,traceorder='normal',font=dict(family='sans-serif',size=20,color='#000'),bgcolor='#E2E2E2',
        bordercolor='#FFFFFF',borderwidth=1))
        fig=go.Figure(data=wt_data_trace,layout=layout)
        if output_path:
            plot(fig,auto_open=False,filename=output_path+graph_title+"-WaitTime",image_filename=graph_title+"-WaitTime",image='png')
        else:
            plot(fig,auto_open=False,filename=graph_title+"-WaitTime",image_filename=graph_title+"-WaitTime",image='png')

#Function to plot group with FF and BP
def plot_ff_bp_group_trace(et_data_trace,wt_data_trace,graph_title):
    print("PLOT_FF_BP_GROUP_TRACE()")
    global medpu_tolerance_list
    label_list=graph_title.split('-')
    label1=label_list[0]
    label2=label_list[1]
    if et_data_trace:
        FF_label1="FF-"+label1+" "+str(FF_et_list_g1[0])+"s"
        BP_label1="BP-"+label1+" "+str(BP_et_list_g1[0])+"s"
        FF_label2="FF-"+label2+" "+str(FF_et_list_g2[0])+"s"
        BP_label2="BP-"+label2+" "+str(BP_et_list_g2[0])+"s"

        trace1=go.Scatter(x=medpu_tolerance_list,y=FF_et_list_g1,name="FF",line=dict(dash = 'dash',width=4,color='#000000'),marker=dict(opacity=0)
        ,showlegend=False)
        trace2=go.Scatter(x=medpu_tolerance_list,y=BP_et_list_g1,name="BP",line=dict(dash = 'dot',width=4,color='#000000'),marker=dict(opacity=0)
        ,showlegend=False)
        trace3=go.Scatter(x=medpu_tolerance_list,y=FF_et_list_g2,name="FF",line=dict(dash = 'dash',width=4,color='#000000'),marker=dict(opacity=0)
        ,showlegend=False)
        trace4=go.Scatter(x=medpu_tolerance_list,y=BP_et_list_g2,name="BP",line=dict(dash = 'dot',width=4,color='#000000'),marker=dict(opacity=0)
        ,showlegend=False)

        et_data_trace.append(trace1)
        et_data_trace.append(trace2)
        et_data_trace.append(trace3)
        et_data_trace.append(trace4)
        if maxpu_tolerance_list:
            medpu_tolerance_list=maxpu_tolerance_list

        # CHANGE AX AND AY FOR LABELS IF YOU WANT TO REPOSITION THEM.
        # CURRENTLY REPOSITIONED THEM FOR THE UPDATED RUNS USED FOR IPDPS 2020 SUBMISSION.
        layout=go.Layout(annotations=[
        dict(x=15,y=FF_et_list_g2[0],xref='x',yref='y',text=FF_label2,
        showarrow=True,font=dict(size=28,color="#000",family='Arial'),arrowhead=1,ax=20,ay=15,arrowside="start",arrowwidth=2),
        dict(x=35,y=BP_et_list_g2[0],xref='x',yref='y',text=BP_label2,
        showarrow=True,font=dict(size=28,color="#000",family='Arial'),arrowhead=1,ax=50,ay=-15,arrowside="start",arrowwidth=2),
        dict(x=10,y=FF_et_list_g1[0],xref='x',yref='y',text=FF_label1,
        showarrow=True,font=dict(size=28,color="#000",family='Arial'),arrowhead=1,ax=0,ay=-60,arrowside="start",arrowwidth=2),
        dict(x=60,y=BP_et_list_g1[0],xref='x',yref='y',text=BP_label1,
        showarrow=True,font=dict(size=28,color="#000",family='Arial'),arrowhead=1,ax=0,ay=-70,arrowside="start",arrowwidth=2)]
        ,title="",yaxis=dict(title="Average Execution Time (seconds)",gridwidth=3,titlefont=dict(size=30,family="Arial",color="#000000"),
        tickfont=dict(size=28,family="Arial",color="#000000"),range=[0,300]),
        xaxis=dict(title='Power_Tolerance (%)',gridwidth=3,dtick=10,tickwidth=4,
        titlefont=dict(size=30,family="Arial",color="#000000"),tickfont=dict(size=28,family="Arial",color="#000000"))
        ,legend=dict(x=0.75,y=0.30
        ,traceorder='normal',font=dict(family='sans-serif',size=24,color='#000'),bgcolor='#E2E2E2',
        bordercolor='#FFFFFF',borderwidth=1.3))
        fig=go.Figure(data=et_data_trace,layout=layout)
        if output_path:
            plot(fig,auto_open=False,filename=output_path+graph_title+"-ExecutionTime",image='png',image_filename=graph_title+"ExecutionTime")
        else:
            plot(fig,auto_open=False,filename=graph_title+"-ExecutionTime",image='png',image_filename=graph_title+"ExecutionTime")
    if wt_data_trace:
        FF_label1="FF-"+label1+" "+str(FF_wt_list_g1[0])+"s"
        BP_label1="BP-"+label1+" "+str(BP_wt_list_g1[0])+"s"
        FF_label2="FF-"+label2+" "+str(FF_wt_list_g2[0])+"s"
        BP_label2="BP-"+label2+" "+str(BP_wt_list_g2[0])+"s"

        trace1=go.Scatter(x=medpu_tolerance_list,y=FF_wt_list_g1,name="FF",line=dict(dash = 'dash',width=4,color='#000000'),marker=dict(opacity=0)
        ,showlegend=False)
        trace2=go.Scatter(x=medpu_tolerance_list,y=BP_wt_list_g1,name="BP",line=dict(dash = 'dot',width=4,color='#000000'),marker=dict(opacity=0)
        ,showlegend=False)
        trace3=go.Scatter(x=medpu_tolerance_list,y=FF_wt_list_g2,name="FF",line=dict(dash = 'dash',width=4,color='#000000'),marker=dict(opacity=0)
        ,showlegend=False)
        trace4=go.Scatter(x=medpu_tolerance_list,y=BP_wt_list_g2,name="BP",line=dict(dash = 'dot',width=4,color='#000000'),marker=dict(opacity=0)
        ,showlegend=False)
        wt_data_trace.append(trace1)
        wt_data_trace.append(trace2)
        wt_data_trace.append(trace3)
        wt_data_trace.append(trace4)
        if maxpu_tolerance_list:
            medpu_tolerance_list=maxpu_tolerance_list

        # CHANGE AX AND AY FOR LABELS IF YOU WANT TO REPOSITION THEM.
        # CURRENTLY CHANGED y VALUE TO POINT TO [FF|BP]_wt_list_g[1|2][0] FOR THE UPDATED RUNS USED FOR IPDPS 2020 SUBMISSION.
        # CHANGE range[...] VALUE IF ALTERING THE SCALE.
        layout=go.Layout(annotations=[
        dict(x=10,y=FF_wt_list_g2[0],xref='x',yref='y',text=FF_label2,
        showarrow=True,font=dict(size=28,color="#000",family='Arial'),arrowhead=1,ax=0,ay=-120,arrowside="start",arrowwidth=2),
        dict(x=75,y=BP_wt_list_g2[0],xref='x',yref='y',text=BP_label2,
        showarrow=True,font=dict(size=28,color="#000",family='Arial'),arrowhead=1,ax=0,ay=-170,arrowside="start",arrowwidth=2),
        dict(x=25,y=FF_wt_list_g1[0],xref='x',yref='y',text=FF_label1,
        showarrow=True,font=dict(size=28,color="#000",family='Arial'),arrowhead=1,ax=0,ay=-25,arrowside="start",arrowwidth=2),
        dict(x=90,y=BP_wt_list_g1[0],xref='x',yref='y',text=BP_label1,
        showarrow=True,font=dict(size=28,color="#000",family='Arial'),arrowhead=1,ax=0,ay=-120,arrowside="start",arrowwidth=2)]
        ,title="",yaxis=dict(title="Average Wait Time (seconds)",titlefont=dict(size=30,family="Arial",color="#000000"),
        tickfont=dict(size=24,family="Arial",color="#000000"),gridwidth=3,range=[0,1500]),
        xaxis=dict(title='Power_Tolerance (%)',gridwidth=3,dtick=10,tickwidth=4,
        titlefont=dict(size=30,family="Arial",color="#000000"),tickfont=dict(size=28,family="Arial",color="#000000"))
        ,legend=dict(x=0.45
        ,traceorder='normal',font=dict(family='sans-serif',size=24,color='#000'),bgcolor='#E2E2E2',
        bordercolor='#FFFFFF',borderwidth=1.3))
        fig=go.Figure(data=wt_data_trace,layout=layout)
        if output_path:
            plot(fig,auto_open=False,filename=output_path+graph_title+"-WaitTime",image='png',image_filename=graph_title+"WaitTime")
        else:
            plot(fig,auto_open=False,filename=graph_title+"-WaitTime",image='png',image_filename=graph_title+"WaitTime")



# Function to plot Power vs Tolerance
def plot_power_tolerance(average_peak_list,max_peak_list,percentile_peak_list,graph_title,group_tasks):
    print("PLOT_POWER_TOLERANCE()")
    global medpu_tolerance_list
    global power_profile_percentile

    if maxpu_tolerance_list:
        medpu_tolerance_list=maxpu_tolerance_list
    #trace0=go.Scatter(x=medpu_tolerance_list,y=average_peak_list,name="Average Power",line=dict(width=3),marker=dict(symbol=100,size=15))
    FF_label1="FF Max Power- "+str(round(max_pow_ff_list[0],2))+"W"
    BP_label1="BP Max Power- "+str(round(max_pow_bp_list[0],2))+"W"
    FF_label2="FF "+str(power_profile_percentile)+" Percentile Power- "+str(round(perc_pow_ff_list[0],2))+"W"
    BP_label2="BP "+str(power_profile_percentile)+" Percentile Power- "+str(round(perc_pow_bp_list[0],2))+"W"
    trace1=go.Scatter(x=medpu_tolerance_list,y=max_peak_list,line=dict(color='#8838D0',width=5),
    name="Maximum Peak",marker=dict(symbol=101,size=24,line=dict(width=3)))
    trace2=go.Scatter(x=medpu_tolerance_list,y=percentile_peak_list,line=dict(width=5),name=str(power_profile_percentile)+" Percentile Power"
    ,marker=dict(symbol=103,size=24,line=dict(width=3)))
    trace3=go.Scatter(x=medpu_tolerance_list,y=max_pow_ff_list,name="FF",line=dict(dash = 'dash',width=4,color='#000000'),marker=dict(opacity=0)
    ,showlegend=False)
    trace4=go.Scatter(x=medpu_tolerance_list,y=max_pow_bp_list,name="BP",line=dict(dash = 'dot',width=4,color='#000000'),marker=dict(opacity=0)
    ,showlegend=False)
    trace5=go.Scatter(x=medpu_tolerance_list,y=perc_pow_ff_list,name="FF",line=dict(dash = 'dash',width=4,color='#000000'),marker=dict(opacity=0)
    ,showlegend=False)
    trace6=go.Scatter(x=medpu_tolerance_list,y=perc_pow_bp_list,name="BP",line=dict(dash = 'dot',width=4,color='#000000'),marker=dict(opacity=0)
    ,showlegend=False)

    # CHANGE AX AND AY FOR LABELS IF YOU WANT TO REPOSITION THEM.
    # CURRENTLY CHANGED y VALUE TO POINT TO [FF|BP]_wt_list_g[1|2][0] FOR THE UPDATED RUNS USED FOR IPDPS 2020 SUBMISSION.
    # CHANGE range[...] VALUE IF ALTERING THE SCALE.
    if group_tasks:
        layout=go.Layout(annotations=[
        dict(x=20,y=perc_pow_ff_list[0],xref='x',yref='y',text=FF_label2,
        showarrow=True,font=dict(size=26,color="#000",family='Arial'),arrowhead=1,ax=-50,ay=-65,arrowside="start",arrowwidth=2),
        dict(x=20,y=perc_pow_bp_list[0],xref='x',yref='y',text=BP_label2,
        showarrow=True,font=dict(size=26,color="#000",family='Arial'),arrowhead=1,ax=0,ay=15,arrowside="start",arrowwidth=2),
        dict(x=85,y=max_pow_ff_list[0],xref='x',yref='y',text=FF_label1,
        showarrow=True,font=dict(size=26,color="#000",family='Arial'),arrowhead=1,ax=0,ay=-35,arrowside="start",arrowwidth=2),
        dict(x=85,y=max_pow_bp_list[0],xref='x',yref='y',text=BP_label1,
        showarrow=True,font=dict(size=26,color="#000",family='Arial'),arrowhead=1,ax=0,ay=-15,arrowside="start",arrowwidth=2)],
        title="",yaxis=dict(title="Power (Watts)",title_standoff=25,gridwidth=3,titlefont=dict(size=30,family="Arial",color="#000000")
        ,tickfont=dict(size=28,family="Arial",color="#000000"),range=[500,1500]),
        xaxis=dict(title='Power_Tolerance (%)',gridwidth=3,dtick=10,tickwidth=4,titlefont=dict(size=30,family="Arial",color="#000000")
        ,tickfont=dict(size=28,family="Arial",color="#000000"),ticktext=["0/100H","25/75H","75/25H","100/0H"]
        ,tickvals=medpu_tolerance_list),legend=dict(x=0.13,y=1.1
        ,traceorder='normal',font=dict(family='sans-serif',size=28,color='#000'),bgcolor='#E2E2E2',
        bordercolor='#FFFFFF',borderwidth=1))
    else:
        layout=go.Layout(annotations=[
        dict(x=50,y=round(perc_pow_ff_list[0],2),xref='x',yref='y',text=FF_label2,
        showarrow=True,font=dict(size=28,color="#000",family='Arial'),arrowhead=1,ax=-90,ay=-15,arrowside="start",arrowwidth=2),
        dict(x=30,y=round(perc_pow_bp_list[0],2),xref='x',yref='y',text=BP_label2,
        showarrow=True,font=dict(size=28,color="#000",family='Arial'),arrowhead=1,ax=0,ay=105,arrowside="start",arrowwidth=2),
        dict(x=70,y=round(max_pow_ff_list[0],2),xref='x',yref='y',text=FF_label1,
        showarrow=True,font=dict(size=28,color="#000",family='Arial'),arrowhead=1,ax=-90,ay=-20,arrowside="start",arrowwidth=2),
        dict(x=80,y=round(max_pow_bp_list[0],2),xref='x',yref='y',text=BP_label1,
        showarrow=True,font=dict(size=28,color="#000",family='Arial'),arrowhead=1,ax=0,ay=-60,arrowside="start",arrowwidth=2)],
        title="",yaxis=dict(tickformat='.2s',title="Power (Watts)",gridwidth=3,titlefont=dict(size=29,family="Arial",color="#000000")
        ,tickfont=dict(size=27,family="Arial",color="#000000"),zerolinecolor='#000000',range=[500,1500]),
        xaxis=dict(title='Power_Tolerance (%)',gridwidth=3,dtick=10,tickwidth=4,titlefont=dict(size=29,family="Arial",color="#000000")
        ,tickfont=dict(size=27,family="Arial",color="#000000")),legend=dict(x=0.53,y=0.1
        ,traceorder='normal',font=dict(family='sans-serif',size=22,color='#000'),bgcolor='#E2E2E2',
        bordercolor='#FFFFFF',borderwidth=1))
    data=[trace1,trace2,trace3,trace4,trace5,trace6]
    fig=go.Figure(data=data,layout=layout)
    if output_path:
        plot(fig,auto_open=False,filename=output_path+graph_title+"-powerVtolerance",image_filename=graph_title+"-powerVtolerance",image='png',image_width=850,image_height=600)
    else:
        plot(fig,auto_open=False,filename=graph_title+"-powerVtolerance",image_filename=graph_title+"-powerVtolerance",image='png',image_width=850,image_height=600)

# Function to plot Graph with Power, Time and Power_Tolerance
def plot_time_power_tolerance(average_peak_list,max_peak_list,percentile_peak_list,medpu_list,maxpu_list,filename,axis_title,title):
    print("PLOT_TIME_POWER_TOLERANCE()")
    global power_profile_percentile
    trace=go.Scatter(x=medpu_tolerance_list,y=medpu_list,line=dict(dash = 'dash',width=3),name="medmedmedpu",marker=dict(symbol=1,size=15))
    trace1=go.Scatter(x=medpu_tolerance_list,y=maxpu_list,line=dict(dash = 'dashdot',width=3),name="medmedmaxpeakpu",marker=dict(symbol=2,size=15))
    trace2=go.Scatter(x=medpu_tolerance_list,y=average_peak_list,line=dict(dash = 'dot',width=3),name="Average Peak",yaxis='y2',marker=dict(symbol=3,size=15))
    trace3=go.Scatter(x=medpu_tolerance_list,y=max_peak_list,line=dict(dash = 'longdash',width=3),name="Max Peak",yaxis='y2',marker=dict(symbol=4,size=15))
    trace4=go.Scatter(x=medpu_tolerance_list,y=percentile_peak_list,line=dict(dash = 'longdashdot',width=3),name=str(power_profile_percentile)+" Percentile"
    ,yaxis='y2',marker=dict(symbol=5,size=15))
    data=[trace,trace1,trace2,trace3,trace4]
    layout=go.Layout(title=title,yaxis=dict(title=axis_title,gridwidth=1,range=[0,5500],gridcolor='#000000'),
    xaxis=dict(title='Power_Tolerance (%)',gridwidth=1,gridcolor='#000000',dtick=10,tickwidth=4),yaxis2=dict(title="Power (Watts)"
    ,gridwidth=1,gridcolor='#000000',side='right',overlaying='y',range=[0,5500]),legend=dict(x=0.45
    ,traceorder='normal',font=dict(family='sans-serif',size=17,color='#000'),bgcolor='#E2E2E2',
    bordercolor='#FFFFFF',borderwidth=1))
    fig=go.Figure(data=data,layout=layout)
    plot(fig,auto_open=False,filename=filename)

# Function to plot Graph with a specific Profile
def plot_metrics_for_profile(prof_wt_list,prof_et_list,prof_tt_list,title):
    print("PLOT_METRICS_FOR_PROFILE()")
    global medpu_tolerance_list
    if maxpu_tolerance_list:
        medpu_tolerance_list=maxpu_tolerance_list
    trace0=go.Scatter(x=medpu_tolerance_list,y=prof_wt_list,name="Average Wait-Time",line=dict(width=3))
    trace1=go.Scatter(x=medpu_tolerance_list,y=prof_et_list,line=dict(color='#8838D0',width=3),name="Average Execution-Time")
    trace2=go.Scatter(x=medpu_tolerance_list,y=prof_tt_list,line=dict(width=3),name="Average Turnaround-Time")
    layout=go.Layout(title=title,yaxis=dict(title="Time (seconds)",gridwidth=1,gridcolor='#000000'),
    xaxis=dict(title='Power_Tolerance (%)',gridwidth=1,gridcolor='#000000',dtick=10,tickwidth=4),legend=dict(x=0.45
    ,traceorder='normal',font=dict(family='sans-serif',size=17,color='#000'),bgcolor='#E2E2E2',
    bordercolor='#FFFFFF',borderwidth=1))
    data=[trace0,trace1,trace2]
    fig=go.Figure(data=data,layout=layout)
    plot(fig,auto_open=False,filename=title+"-metrics.html")

# Plot tolerance for both profiles
def plot_tolerance(medpu_list,maxpu_list,title,file_name,axis_title):
    print("PLOT_TOLERANCE()")
    trace=go.Scatter(x=medpu_tolerance_list,y=medpu_list,line=dict(color='#8838D0'),name="medmedmedpu")
    trace1=go.Scatter(x=maxpu_tolerance_list,y=maxpu_list,line=dict(name="medmedmaxpeakpu"))
    layout=go.Layout(title=title,yaxis=dict(title=axis_title,gridwidth=1,gridcolor='#000000'),
    xaxis=dict(title='Power_Tolerance (%)',gridwidth=1,gridcolor='#000000',dtick=10,tickwidth=4),legend=dict(x=0.45
    ,traceorder='normal',font=dict(family='sans-serif',size=13,color='#000'),bgcolor='#E2E2E2',
    bordercolor='#FFFFFF',borderwidth=1))
    data=[trace,trace1]
    fig=go.Figure(data=data,layout=layout)
    plot(fig,auto_open=False,filename=file_name)

def plot_profile_and_metric(metric_list,graph_title,name_metric,filename):
    print("PLOT_PROFILE_METRIC()")
    global medpu_tolerance_list
    if "Wait" in name_metric:
        leg_title="Average wT"
        trace1=go.Scatter(x=medpu_tolerance_list,y=FF_wt_list,name="FF",line=dict(dash = 'dash',width=3)
        ,marker=dict(opacity=0),mode='lines+text',text=["",FF_wt_list[0],"","",""],textposition='top center',
        textfont={"size":[20,20,20,20,20]})
        trace2=go.Scatter(x=medpu_tolerance_list,y=BP_wt_list,name="BP",line=dict(dash = 'dot',width=3)
        ,marker=dict(opacity=0),mode='lines+text',text=["",BP_wt_list[0],"","",""],textposition='top center',
        textfont={"size":[20,20,20,20,20]})
    if "Execution" in name_metric:
        leg_title="Average eT"
        trace1=go.Scatter(x=medpu_tolerance_list,y=FF_et_list,name="FF",line=dict(dash = 'dash',width=3),marker=dict(opacity=0)
        ,mode='lines+text',text=["",FF_et_list[0],"","",""],textposition='top center',
        textfont={"size":[20,20,20,20,20]})
        trace2=go.Scatter(x=medpu_tolerance_list,y=BP_et_list,name="BP",line=dict(dash = 'dot',width=3),marker=dict(opacity=0)
        ,mode='lines+text',text=["",BP_et_list[0],"","",""],textposition='top center',
        textfont={"size":[20,20,20,20,20]})
    if maxpu_tolerance_list:
        medpu_tolerance_list=maxpu_tolerance_list
    trace0=go.Scatter(x=medpu_tolerance_list,y=metric_list,name=leg_title,marker=dict(symbol=100,size=20,line=dict(width=3)))
    layout=go.Layout(title="",yaxis=dict(title=name_metric+" (seconds)",gridwidth=1,gridcolor='#000000',titlefont=dict(size=26),
    tickfont=dict(size=19)),
    xaxis=dict(title='Power_Tolerance (%)',gridwidth=1,gridcolor='#000000',dtick=10,tickwidth=4,
    titlefont=dict(size=26),tickfont=dict(size=19))
    ,legend=dict(x=0.45
    ,traceorder='normal',font=dict(family='sans-serif',size=20,color='#000'),bgcolor='#E2E2E2',
    bordercolor='#FFFFFF',borderwidth=1.3))
    data=[trace0,trace1,trace2]
    fig=go.Figure(data=data,layout=layout)
    plot(fig,auto_open=False,filename=filename,image='png',image_filename=filename)

def plot_metrics(plot_task_tt_list,plot_task_wt_list,plot_task_et_list,plot_for_task,power_plot,png_file):
    print("PLOT_METRICS()")
    global num_powerclasses

    fList = file_names[0::2]
    size=len(fList)
    if  plot_for_task and power_plot:
        data=[]
        i=0
        while i<size:
            j=0
            data_list_execution_time = [
                    task_exec_map[plot_for_task][i][j],
                    task_exec_map[plot_for_task][i][j+1],
                    task_exec_map[plot_for_task][i][j+2]
            ]

            data_list_turnaround_time = [
                    task_turn_map[plot_for_task][i][j],
                    task_turn_map[plot_for_task][i][j+1],
                    task_turn_map[plot_for_task][i][j+2]
            ]

            if num_powerclasses == 4:
                data_list_execution_time.append(task_exec_map[plot_for_task][i][j+3])
                data_list_turnaround_time.append(task_turn_map[plot_for_task][i][j+3])

            data_list=[plot_task_wt_list[i]]
            data_list.extend(data_list_execution_time)
            data_list.extend(data_list_turnaround_time)

            execution_time_x_arr = ['ET-A','ET-B','ET-C']
            turnaround_time_x_arr = ['TAT-A','TAT-B','TAT-C']

            if num_powerclasses == 4:
                execution_time_x_arr.append('ET-D')
                turnaround_time_x_arr.append('TAT-D')

            x_arr = ['WT']
            x_arr.extend(execution_time_x_arr)
            x_arr.extend(turnaround_time_x_arr)
            trace=go.Bar(x=x_arr,y=data_list,name=fList[i])

            data.append(trace)
            i=i+1
        layout = go.Layout(barmode='group',yaxis=dict(title='Time (seconds)'),xaxis=dict(tickangle=0),
        titlefont=dict(size=18,color="#000000"),title=plot_for_task.replace("electron-"," "))
        fig=go.Figure(data=data,layout=layout)
    elif plot_for_task:
        data=[]
        i=0
        while i<size:
            data_list=[plot_task_wt_list[i],plot_task_et_list[i],plot_task_tt_list[i]]
            trace=go.Bar(x=['Wait Time','Execution Time','Turnaround Time'],y=data_list,name=fList[i])
            data.append(trace)
            i=i+1
        layout = go.Layout(barmode='group',yaxis=dict(title='Time (seconds)'),xaxis=dict(tickangle=0,automargin=True),
        titlefont=dict(size=18,color="#000000"),title=plot_for_task.replace("electron-"," "))
        fig=go.Figure(data=data,layout=layout)
    else:
        data=[]
        i=0
        while i<size:
            data_list=[overall_wt_list[i],overall_et_list[i],overall_list[i]]
            trace=go.Bar(x=['Wait Time','Execution Time','Turnaround Time'],y=data_list,name=fList[i])
            data.append(trace)
            i=i+1
        layout = go.Layout(barmode='group',yaxis=dict(title='Time (seconds)'),titlefont=dict(size=18,color="#000000"),title="Overall Averages")
        fig=go.Figure(data=data,layout=layout)

    if png_file:
        plot(fig,image='png',auto_open=False,image_filename=png_file)
    else:
        plot(fig,auto_open=False)

#Calculate Average Turn Around Time for a file
def allTasks_avg_turn_time(metrics_df,out,key):
    print("ALLTASKS_AVG_TURN_TIME()")
    wait_times_list=metrics_df[headers[2]]
    exec_times_list=metrics_df[headers[-1]]
    turnaround_time_list=[sum(x) for x in zip(wait_times_list, exec_times_list)]
    overall_list.append(round(np.average(turnaround_time_list),3))
    if 'medmedmedpu' in key:
        medpu_overall_tt_list.append(round(np.average(turnaround_time_list),3))
    elif 'medmedmaxpeakpu' in key:
        maxpu_overall_tt_list.append(round(np.average(turnaround_time_list),3))
    file=open(out,"a")
    file.write(key+": "+str(round(np.average(turnaround_time_list),3))+"\n")
    file.close()

#Calculate Average Wait Time for the whole file
def allTasks_avg_wait_time(metrics_df,out,key):
    print("ALLTASKS_AVG_WAIT_TIME()")
    wait_times_list=metrics_df[headers[2]]
    overall_wt_list.append(round(np.average(wait_times_list),3))
    if 'medmedmedpu' in key:
        medpu_overall_wt_list.append(round(np.average(wait_times_list),3))
    elif 'medmedmaxpeakpu' in key:
        maxpu_overall_wt_list.append(round(np.average(wait_times_list),3))
    file=open(out,"a")
    file.write(key+": "+str(round(np.average(wait_times_list),3))+"\n")
    file.close()

#Calculate Average Execution Time
def allTasks_execution_time(metrics_df,out,key):
    print("ALLTASKS_EXECUTION_TIME()")
    exec_times_list=metrics_df[headers[-1]]
    overall_et_list.append(round(np.average(exec_times_list),3))
    if 'medmedmedpu' in key:
        medpu_overall_et_list.append(round(np.average(exec_times_list),3))
    elif 'medmedmaxpeakpu' in key:
        maxpu_overall_et_list.append(round(np.average(exec_times_list),3))
    file=open(out,"a")
    file.write(key+": "+str(round(np.average(exec_times_list),3))+"\n")
    file.close()

#Function to calculate average wait time per task
def per_task_cluster_averages():
    print("PER_TASK_CLUSTER_AVERAGES()")
    global file_map
    global taskList
    global file_map
    global task_average_map
    var_exec_map_list=list()
    var_turn_map_list=list()
    i=0
    #Iterate over all files entered
    while i <(len(runtime_docs_list)):
        task_list_with_ID=runtime_docs_list[i][headers[0]];
        waitTimes=runtime_docs_list[i][headers[2]];
        execTimes=runtime_docs_list[i][headers[-1]]
        turn_times=[sum(x) for x in zip(waitTimes, execTimes)]
        #Gets the map having (task_name : Average) for the file for all tasks
        var_map=get_averages(task_list_with_ID,waitTimes,execTimes,turn_times)
        for key,value in var_map.items():
            temp=[key,value]
            var_map_list.append(temp)
        for key,value in exec_pertask_avg_map.items():
            temp=[key,value]
            var_exec_map_list.append(temp)
        for key,value in turn_pertask_avg_map.items():
            temp=[key,value]
            var_turn_map_list.append(temp)
        i=i+1
        #print(var_map_list)
    for t in taskList:
        task_average_map[t]=[]
        task_average_exec_map[t]=[]
        task_average_turn_map[t]=[]

    #Combining and comparing all averages from all files of all tasks
    for taskN in taskList:
        for var in var_map_list:
                if taskN in var:
                    temp_list=task_average_map[taskN]
                    temp_list.append(var[1])
                    task_average_map[taskN]=temp_list

    for taskN in taskList:
        for var in var_exec_map_list:
                if taskN in var:
                    temp_list=task_average_exec_map[taskN]
                    temp_list.append(var[1])
                    task_average_exec_map[taskN]=temp_list
    for taskN in taskList:
        for var in var_turn_map_list:
                if taskN in var:
                    temp_list=task_average_turn_map[taskN]
                    temp_list.append(var[1])
                    task_average_turn_map[taskN]=temp_list

def get_averages(task_list_with_ID,waitTimes,execTimes,turn_times):
    print("GET_AVERAGES()")

    global taskList
    #global exec_times_map
    wt_map_original=dict()
    et_map_original=dict()
    tt_map_original=dict()
    #Get names of tasks and make a list of unique task names
    for task in task_list_with_ID:
        ind=re.search(r"-[0-9]*-",task).start()
        taskName=task[0:ind]
        taskList.append(taskName)
    taskList = list(dict.fromkeys(taskList))
    for t in taskList:
        wt_map_per_task[t]=[]
        exec_pertask_avg_map[t]=[]
        turn_pertask_avg_map[t]=[]
    i=0
    #Map of task instances and their respective WTs and ETs
    while i<len(task_list_with_ID):
        task_name=task_list_with_ID[i]
        wt_map_original[task_name]=waitTimes[i]
        et_map_original[task_name]=execTimes[i]
        tt_map_original[task_name]=turn_times[i]
        i=i+1
    #Append list of WTs for each instance to the taskname and make a map
    for taskN in taskList:
            for key,value in wt_map_original.items():
                if taskN in key:
                    wt_list=wt_map_per_task[taskN]
                    wt_list.append(value)
                    wt_map_per_task[taskN]=wt_list
    for taskN in taskList:
            for key,value in et_map_original.items():
                if taskN in key:
                    et_list=exec_pertask_avg_map[taskN]
                    et_list.append(value)
                    exec_pertask_avg_map[taskN]=et_list
    for taskN in taskList:
            for key,value in tt_map_original.items():
                if taskN in key:
                    tt_list=turn_pertask_avg_map[taskN]
                    tt_list.append(value)
                    turn_pertask_avg_map[taskN]=tt_list

    for taskN in taskList:
        for key,value in exec_pertask_avg_map.items():
            if taskN in key:
                et_list=exec_pertask_avg_map[taskN]
                exec_pertask_avg_map[taskN]=round(np.average(et_list),2)
    for taskN in taskList:
        for key,value in turn_pertask_avg_map.items():
            if taskN in key:
                tt_list=turn_pertask_avg_map[taskN]
                turn_pertask_avg_map[taskN]=round(np.average(tt_list),2)
    #Get average of all tasks
    for taskN in taskList:
        for key,value in wt_map_per_task.items():
            if taskN in key:
                wt_list=wt_map_per_task[taskN]
                wt_map_per_task[taskN]=round(np.average(wt_list),2)

    return wt_map_per_task

def perTask_perClass_avg_exec_time():
    print("PERTASK_PERCLASS_AVG_EXEC_TIME()")

    i=0
    for t in taskList:
        task_exec_map[t]=[]
        task_turn_map[t]=[]
    #Iterate over all files entered
    while i<(len(sched_docs_list)):
        instanceList=sched_docs_list[i][schedTraceHeaders[2]]
        execTimes=runtime_docs_list[i][headers[-1]]
        wait_times_list=runtime_docs_list[i][headers[2]]
        turnaround_time_list=[sum(x) for x in zip(wait_times_list, execTimes)]
        task_list_with_ID=runtime_docs_list[i][headers[0]];
        j=0
        while j<len(task_list_with_ID):
            task_name=task_list_with_ID[j]
            exec_times_map[task_name]=execTimes[j]
            turn_time_map[task_name]=turnaround_time_list[j]
            j=j+1

        ClassA=ClassB=ClassC=ClassList=list()
        ClassD=None
        ClassList.append(ClassA)
        ClassList.append(ClassB)
        ClassList.append(ClassC)

        ClassA_turn=ClassB_turn=ClassC_turn=ClassList_turn=list()
        ClassD_turn=None
        ClassList_turn.append(ClassA_turn)
        ClassList_turn.append(ClassB_turn)
        ClassList_turn.append(ClassC_turn)

        if num_powerclasses == 4:
            ClassD = list()
            ClassD_turn = list()
            ClassList.append(ClassD)
            ClassList_turn.append(ClassD_turn)

        #Iterate over all tasks in the list of tasks

        for task_name in taskList:
            for i in range(len(ClassList)):
                ClassList[i] = []
            for i in range(len(ClassList_turn)):
                ClassList_turn[i] = []

            powerClass_map=[]
            classname_to_index = {"A": 0, "B": 1, "C": 2}
            if num_powerclasses == 4:
                classname_to_index["D"] = 3

            #Iterate over a map which has a taskname and its executionTimes
            for task_name_withID,exec_times, in exec_times_map.items():
                if task_name in task_name_withID:
                    #Iterate over instance list obtained from the schedTrace
                    for inst in instanceList:
                        if task_name_withID in inst:
                            #Allot classes to the instances
                            for className,hosts_list in powerClass.items():
                                for host in hosts_list:
                                    if host in inst:
                                        ClassList[classname_to_index[className]].append(exec_times)
                                        ClassList_turn[classname_to_index[className]].append(turn_time_map[task_name_withID])
            list_averages=list()
            for class_list in ClassList:
                list_averages.append(round(np.average(class_list), 2))

            turn_list_averages=list()
            for class_list_turn in ClassList_turn:
                turn_list_averages.append(round(np.average(class_list_turn), 2))

            tempList=task_exec_map[task_name]
            tempTurnList=task_turn_map[task_name]
            tempList.append(list_averages)
            tempTurnList.append(turn_list_averages)
            task_exec_map[task_name]=tempList
            task_turn_map[task_name]=tempTurnList


        i=i+1

def write_power_stats(average_peak_list,max_peak_list,percentile_peak_list,out):
    print("WRITE_POWER_STATS()")
    global power_profile_percentile
    file=open(out,"a")
    file.write("Average Peak Power"+"\n")
    file.close()
    with open(out, 'a') as f:
        for item in average_peak_list:
            f.write('%s\n' % item)
    with open(out, 'a') as f:
        f.write("Max Peak Power"+"\n")
        for item in max_peak_list:
            f.write('%s\n' % item)
    with open(out, 'a') as f:
        f.write(str(power_profile_percentile)+" Percentile Peak Power"+"\n")
        for item in percentile_peak_list:
            f.write('%s\n' % item)

def write_results(out,plot_for_task,plot_task_tt_list,plot_task_wt_list,plot_task_et_list,runtime_dir_list,sched_dir_list,mode,plot_tasks):
    print("WRITE_RESULTS()")

    global num_powerclasses
    #print(plot_for_task)

    file=open(out,"a")
    file.write("Per Task Stats"+"\n"+"========================"+"\n")
    for key,value in task_average_map.items():
        file.write("task-name: "+key+"\n"+"------------------------"+"\n")
        i=0
        j=0
        file.write("Average Wait Time:"+"\n")
        for v in value:
            if mode=="f":
                file.write(file_names[i]+": "+str(round(v,3))+"\n")
            elif mode=="d":
                file.write(runtime_dir_list[j]+": "+str(round(v,3))+"\n")
                if key==plot_for_task:
                    if 'medmedmedpu' in runtime_dir_list[j]:
                        medpu_fortask_wt_list.append(round(v,3))
                    elif 'medmedmaxpeakpu' in runtime_dir_list[j]:
                        maxpu_fortask_wt_list.append(round(v,3))
            if key==plot_for_task:
                plot_task_wt_list.append(round(v,3))
            i=i+2
            j=j+1
        file.write("\n"+"Average Execution Time:"+"\n")
        j=0
        k=0
        l=0
        while j<len(task_exec_map[key]):
            if mode=="f":
                file.write("File Name: "+file_names[k]+"\n")
            elif mode=="d":
                file.write(runtime_dir_list[j]+"\n")
            
            exec_times_string = "\t"+"Class A:"+str(task_exec_map[key][j][l]).replace('nan','0.0')+"\n\t"+"Class B:"+str(task_exec_map[key][j][l+1]).replace('nan','0.0')+"\n\t"+"Class C:"+str(round(task_exec_map[key][j][l+2],3)).replace('nan','0.0')

            if num_powerclasses == 4:
                exec_times_string += "\n\tClass D:"+str(task_exec_map[key][j][l+3]).replace('nan','0.0')

            # Adding cluster wide execution time data.
            exec_times_string += "\n\t"+"Cluster wide: "+str(task_average_exec_map[key][j]).replace('nan','0.0')+"\n"

            # Writing execution time data to file.
            file.write(exec_times_string)

            if key==plot_for_task:
                plot_task_et_list.append(task_average_exec_map[key][j])
            if key==plot_for_task and mode=="d":
                if 'medmedmedpu' in runtime_dir_list[j]:
                    medpu_fortask_et_list.append(task_average_exec_map[key][j])
                elif 'medmedmaxpeakpu' in runtime_dir_list[j]:
                    maxpu_fortask_et_list.append(task_average_exec_map[key][j])
            k=k+2
            j=j+1
        file.write("\n"+"Average Turnaround Time:"+"\n")
        j=0
        k=0
        l=0
        while j<len(task_turn_map[key]):
            if mode=="f":
                file.write("File Name: "+file_names[k]+"\n")
            elif mode=="d":
                file.write(runtime_dir_list[j]+"\n")

            tasks_tat_string = "\t"+"Class A:"+str(task_turn_map[key][j][l]).replace('nan','0.0')+"\n\t"+"Class B:"+str(task_turn_map[key][j][l+1]).replace('nan','0.0')+"\n\t"+"Class C:"+str(task_turn_map[key][j][l+2]).replace('nan','0.0')

            if num_powerclasses == 4:
                tasks_tat_string += "\n\tClass D:"+str(task_turn_map[key][j][l+3]).replace('nan','0.0')

            # Adding cluster wide task turnaround time data.
            tasks_tat_string += "\n\t"+"Cluster wide: "+str(task_average_turn_map[key][j]).replace('nan','0.0')+"\n"

            # Writing tasks turnaround time data to file.
            file.write(tasks_tat_string)

            if key==plot_for_task:
                plot_task_tt_list.append(task_average_turn_map[key][j])
            if key==plot_for_task and mode=="d":
                if 'medmedmedpu' in runtime_dir_list[j]:
                    medpu_fortask_tt_list.append(task_average_turn_map[key][j])
                elif 'medmedmaxpeakpu' in runtime_dir_list[j]:
                    maxpu_fortask_tt_list.append(task_average_turn_map[key][j])
            k=k+2
            j=j+1
        file.write("\n")


if __name__ == '__main__':
    main()
