import pandas as pd
import warnings
import re
import numpy as np
import csv
import argparse

np.seterr(all='ignore')
warnings.filterwarnings("ignore", category=RuntimeWarning)
#Create lists for headers
headers=['TaskID','submitTime','WaitTime','ExecStartTime','ExecFinalTime','ExecTime']
schedTraceHeaders=['date','time','instanceID']

powerClass = {
                "A":["stratos-005","stratos-006"],
                "B":["stratos-007"],
                "C":["stratos-008"],
                "D":["stratos-001","stratos-002","stratos-003","stratos-004"]
}
taskList=list()
exec_times_map=dict()
wt_map_per_task={}

#Main function
def main():
    parser=argparse.ArgumentParser(description='Files for variance in Execution and Waiting Time')
    parser.add_argument("--runTimeMetricsFile",help="runtimeMetrics.log file")
    parser.add_argument("--schedTraceFile",help="schedTrace.log file")
    parser.add_argument("--output_file",help="Text file to display variance in WTs")
    args=parser.parse_args()
    runTimeMetricsFile=args.runTimeMetricsFile
    schedTraceFile=args.schedTraceFile
    output_file=args.output_file
    #Read input from the files
    metrics_df=pd.read_csv(runTimeMetricsFile,skiprows=1,sep=",",names=headers)
    schedFrame_df=pd.read_csv(schedTraceFile,sep=" ",names=schedTraceHeaders)
    #Populate list of tasks,instances from schedTrace,Exec_times from runtimeMetrics
    task_list_with_ID=metrics_df[headers[0]];
    instanceList=schedFrame_df[schedTraceHeaders[2]]
    execTimes=metrics_df[headers[-1]]
    waitTimes=metrics_df[headers[2]]
    wait_time_variance(task_list_with_ID,waitTimes,execTimes)
    exec_time_variance(output_file,execTimes,instanceList)

def exec_time_variance(output_file,execTimes,instanceList):
    ClassA=ClassB=ClassC=ClassD=ClassList=list()
    ClassList.append(ClassA)
    ClassList.append(ClassB)
    ClassList.append(ClassC)
    ClassList.append(ClassD)
    sample_file=open(output_file,'w')
    #Iterate over all tasks in the list of tasks
    for task_name in taskList:
        ClassList[0]=[];ClassList[1]=[];ClassList[2]=[];ClassList[3]=[];
        powerClass_map=[]
        #Iterate over a map which has a taskname and its executionTimes
        for task_name_withID,exec_times in exec_times_map.items():
            if task_name in task_name_withID:
                #Iterate over instance list obtained from the schedTrace
                for inst in instanceList:
                    if task_name_withID in inst:
                        #Allot classes to the instances
                        for className,hosts_list in powerClass.items():
                            for host in hosts_list:
                                if host in inst:
                                    if className=='A':
                                        ClassList[0].append(exec_times)
                                    elif className=='B':
                                        ClassList[1].append(exec_times)
                                    elif className=='C':
                                        ClassList[2].append(exec_times)
                                    else:
                                        ClassList[3].append(exec_times)
        wait_time_var=str(round(np.var(wt_map_per_task[task_name]),3))
        powerClass_map=[task_name,'Variance in Wait-Time: '+wait_time_var,'Variance in Execution-Time','power-class A: '
        +str(round(np.var(ClassList[0]),3)),'power-class B: '+str(round(np.var(ClassList[1]),3)),
        'power-class C: '+str(round(np.var(ClassList[2]),3)),
        'power-class D: '+str(round(np.var(ClassList[3]),3)),'']
        powerClass_map = [w.replace('nan', '0.0') for w in powerClass_map]
        write_to_file(powerClass_map,output_file)

def wait_time_variance(task_list_with_ID,waitTimes,execTimes):
    global taskList
    global exec_times_map
    wt_map_original=dict()
    #Get names of tasks and make a list of unique task names
    for task in task_list_with_ID:
        tempIndex=task.index('-');
        temp2Index=task.index('-',tempIndex+1)
        taskName=task[0:temp2Index]
        taskList.append(taskName)
    taskList = list(dict.fromkeys(taskList))
    for t in taskList:
        wt_map_per_task[t]=[]
    i=0
    #Map of task instances and their respective WTs and ETs
    while i<len(task_list_with_ID):
        task_name=task_list_with_ID[i]
        wt_map_original[task_name]=waitTimes[i]
        exec_times_map[task_name]=execTimes[i]
        i=i+1
    #Append list of WTs for each instance to the taskname and make a map
    for taskN in taskList:
            for key,value in wt_map_original.items():
                if taskN in key:
                    wt_list=wt_map_per_task[taskN]
                    wt_list.append(value)
                    wt_map_per_task[taskN]=wt_list

def write_to_file(powerClass_map,output_file):
    with open(output_file,mode='a') as sample_file:
        sampleWriter=csv.writer(sample_file,delimiter='\n')
        sampleWriter.writerow(powerClass_map)

if __name__ == '__main__':
  main()
