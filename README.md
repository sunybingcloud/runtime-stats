# runtime-stats


## Instructions to acquire using the Command Builder "plot_perforamnce_metrics_wrt_tolerance.py "
```commandline
usage: plot_performance_metrics_wrt_tolerance.py [-h]
                                                 [--power-intensive-tasks]
                                                 [--non-power-intensive-tasks]
                                                 [--low-et-tasks]
                                                 [--critical-tasks]
                                                 [--high-et-tasks]
                                                 [--all-tasks]
                                                 [--profile-method PROFILE_METHOD]
                                                 [--plot-execution-time]
                                                 [--plot-wait-time]
                                                 [--plot-power-tolerance]
                                                 [--power-profile-percentile POWER_PROFILE_PERCENTILE]
                                                 [--plot-align] [--plot-all]
                                                 [--tasks-group]
                                                 [--title TITLE]
                                                 --out-filename OUT_FILENAME
                                                 --logdir-loc LOGDIR_LOC
                                                 [--ffbp-loc [FFBP_LOC [FFBP_LOC ...]]]
                                                 --logdirprefix LOGDIRPREFIX
                                                 [--output-path OUTPUT_PATH]
                                                 [--wtol-usage-in-filename WTOL_USAGE_IN_FILENAME]
                                                 [--prefix-before-wtol-in-filename PREFIX_BEFORE_WTOL_IN_FILENAME]
                                                 [--num-powerclasses NUM_POWERCLASSES]
                                                 [--powerclass]
                                                 [--plot-dram-power]

Plot performance metrics against tolerance.

optional arguments:
  -h, --help            show this help message and exit
  --power-intensive-tasks
                        Plot performance metrics for power-intensive tasks.
  --non-power-intensive-tasks
                        Plot performance metrics for non power-intensive
                        tasks.
  --low-et-tasks        Plot performance metrics for Low Execution Time tasks.
  --critical-tasks      Plot performance metrics for Critical tasks.
  --high-et-tasks       Plot performance metrics for High Execution Time
                        Tasks.
  --all-tasks           Plot performance metrics for all tasks.
  --profile-method PROFILE_METHOD
                        Method used to estimate watts for tasks. This will be
                        used as the key.
  --plot-execution-time
                        Plot execution time vs tolerance.
  --plot-wait-time      Plot wait time vs tolerance.
  --plot-power-tolerance
                        Plot Tolerance vs Power
  --power-profile-percentile POWER_PROFILE_PERCENTILE
                        Percentile of the peak power consumption to plot
  --plot-align          Plot for all Alignments
  --plot-all            Plot Tolerance vs Power vs Time
  --tasks-group         Task to plot vs Rest
  --title TITLE         Title for graph
  --out-filename OUT_FILENAME
                        Filename to store results in.
  --logdir-loc LOGDIR_LOC
                        Path to directory containing log directories.
  --ffbp-loc [FFBP_LOC [FFBP_LOC ...]]
                        Path to directory containing log directories.
  --logdirprefix LOGDIRPREFIX
                        Prefix common to all log directories being used to
                        plot.
  --output-path OUTPUT_PATH
                        Path for html files
  --wtol-usage-in-filename WTOL_USAGE_IN_FILENAME
                        String used to represent Watts Tolerance in Electron
                        log filenames.
  --prefix-before-wtol-in-filename PREFIX_BEFORE_WTOL_IN_FILENAME
                        Prefix used prior to watts tolerance specification in
                        filename.
  --num-powerclasses NUM_POWERCLASSES
                        Number of power classes
  --powerclass          Plot for power classes and wait time
  --plot-dram-power     Include dram power readings in power plots
```

### Instructions to run to get different graphs

**Remember to separate the title by a hyphen(-)**

Below is the list of abbreviations and their expansions.
1. **HeT** - High Execution Time.
2. **LeT** - Low Execution Time.
3. **PI** - Power Intensive.
4. **NPI** - Non Power Intensive.
5. **HI** - High Impact (select few tasks that are PI and HeT).
6. **critical-tasks** - High Impact tasks.
7. **eT** - Execution Time.
8. **wT** - Wait Time.

Below are 3 commandline options that you would need to specify to make sure that the correct graph is generated.
1. `--wtol-usage-in-filename`.
2. `--prefix-before-wtol-in-filename`.
3. `--num-powerclasses`.
4. `--plot-dram-power`.

If plotting power profiles, by default 90th percentile is used. If a different percentile needs to be set, then
use the `--power-profile-percentile` commandline option to do so. In addition, if the DRAM power readings are
to be included in the power plots, then use the `--plot-dram-power` commandline option.

* For Wait Times and Execution Times of LeT vs HeT with FF and BP
```commandline
python3 plot_performance_metrics_wrt_tolerance.py \
	--logdirprefix <common prefix for log directories> \
	--logdir-loc <Path to directory containing logs> \
	--out-filename "waitTime-execTime_let-het-vs-ff-vs-bp" \
	--title "LeT-HeT" \
	--plot-execution-time --plot-wait-time \
	--low-et-tasks --tasks-group \
	--profile-method medmemdedpu \
	--ffbp-loc <Log directory for First-Fit> <Log directory for Bin-Packing>
```

* For Wait Times and Execution Times of PI vs NPI with FF and BP
```commandline
Remember to separate the title by a hyphen(-)

python3 plot_perforamnce_metrics_wrt_tolerance.py \
	--logdirprefix <common prefix for log directories> \
	--logdir-loc <Path to directory for No ALignment> \
	--out-filename "output.txt" \
	 --title "PI-NPI" \ 
	--plot-execution-time --plot-wait-time \
	--power-intensive-tasks --tasks-group \
	--profile-method medmedmedpu \
	--ffbp-loc <FF-Directory> <BP-Directory-Location>
```

* For Wait Times and Execution Times of Critical Tasks with FF and BP
```commandline
python3 plot_perforamnce_metrics_wrt_tolerance.py \
	--logdirprefix <common prefix for log directories> \
	--logdir-loc <Path to directory containing logs> \
	--out-filename "waitTime-execTime_criticalTasks-vs-ff-vs-bp" \
	--title "HI-LI" \
	--critical-tasks --tasks-group \
	--plot-execution-time --plot-wait-time \
	--profile-method medmedmedpu \
	--ffbp-loc <Log directory for First-Fit> <Log directory for Bin-Packing>
```

* For Power vs Toleranc of No-Alignment with FF and BP
```commandline
python3 plot_perforamnce_metrics_wrt_tolerance.py \
	--logdirprefix <common prefix for log directories> \
	--logdir-loc <Path to directory containing logs> \
	--out-filename "waar-vs-ff-vs-bp_power-vs-tolerance-all-tasks" \
	--title "waar-vs-ff-vs-bp_power-vs-tolerance-all-tasks" \
	--plot-power-tolerance \
	--profile-method medmedmedpu \
	--ffbp-loc <Log directory for First-Fit> <Log Directory Bin-Packing>
```

* For Power vs Tolerance of Critical Tasks with FF and BP
```commandline
python3 plot_perforamnce_metrics_wrt_tolerance.py \
	--logdirprefix <common prefix for log directories> \
	--logdir-loc <Path to directory containing logs> \
	--out-filename "waar-vs-ff-vs-bp_power-vs-tolerance-critical-tasks" \
	--title "waar-vs-ff-vs-bp_power-vs-tolerance-critical-tasks" \
	--plot-power-tolerance \
	--critical-tasks --tasks-group \
	--ffbp-loc <Log directory for First-Fit> <Log Directory Bin-Packing>
```

* For comparing Wait Times and Execution Times of Alignments
```commandline
python3 plot_perforamnce_metrics_wrt_tolerance.py \
	--logdir-loc <Path-to-all-alignment-directories-in-one-directory> \
	--logdirprefix <common prefix for log directories> \
	--profile-method medmedmedpu \
	--out-filename "alignment-comparison-performance" \
	--plot-align \
	--title "Alignment-Comparison"
```

* For comparing Power vs Tolerance for all alignments
```commandline
python3 plot_perforamnce_metrics_wrt_tolerance.py \
	--logdir-loc <Path-to-all-alignment-directories> \
	--logdirprefix <common prefix for log directories> \
	--profile-method medmedmedpu \
	--out-filename "alignment-comparison-power-vs-tolerance" \
	--plot-power-tolerance-align \
	--plot-power-tolerance \
	--title "Power-vs-Tolerance"
```

* Command to singling out a task/set of tasks vs the rest of the tasks\_group
```commandline
python3 plot_perforamnce_metrics_wrt_tolerance.py \
	--logdir-loc <path-to-log-directories> \
	--logdirprefix <common prefix for log directories> \
	--profile-method medmedmedpu \
	--out-filename "video-encoding-vs-otherTasks-performance" \
	--tasks-group electron-video-encoding \
	--plot-wait-time --plot-execution-time \
	--title "videoEncoding-OtherTasks"
```

* Command to obtain Power vs Tolerance Graph for all tasks:
```commandline
python3 plot_perforamnce_metrics_wrt_tolerance.py \
	--logdir-loc <path-to-directories> \
	--logdirprefix <common prefix for log directories> \
	--profile-method medmedmedpu \
	--out-filename "power-vs-tolerance-all-tasks" \
	--plot-power-tolerance --title "power-vs-tolerance-all-tasks"
```

* Command to obtain a Graph for a Set of Tasks (Power intensive/Non Power intensive) and a Single Profiling Method:
```commandline
python3 plot_perforamnce_metrics_wrt_tolerance.py \
	--logdir-loc <path-to-directories> \
	--logdirprefix <common prefix for log directories> \
	--out-filename "power-vs-tolerance-PITasks"
	--power-intensive-tasks \
	--profile-method medmedmedpu \
	--title "power-vs-tolerance-PITasks"
```

* Command to obtain a Graph for All Tasks and a Single Profiling Method:
```commandline
python3 plot_perforamnce_metrics_wrt_tolerance.py \
	--logdir-loc <path-to-directories> \
	--logdirprefix <common prefix for log directories> \
	--all-tasks \
	--out-filename out.txt \
	--profile-method medmedmedpu --title "title"
```

* Command to obtain a Graph for a Set of Tasks (Power intensive/Non Power intensive) for Both Profiling Methods and All Metrics:
```commandline
python3 plot_perforamnce_metrics_wrt_tolerance.py \
	--logdir-loc <path-to-directories>
	--logdirprefix <common prefix for log directories> \
	--out-filename out.txt \
	--non-power-intensive-tasks \
	--plot-all \
	--title "title"
```

* Command to obtain a Graph for All Tasks for Both Profiling Methods and All Metrics:
```commandline
python3 plot_perforamnce_metrics_wrt_tolerance.py \
	--logdir-loc <path-to-directories> \
	--logdirprefix <common prefix for log directories> \
	--out-filename out.txt \
	--all-tasks \
	--plot-all --plot-wait-time --plot-execution-time \
	--title "title"
```

* Command to obtain a Graph for Set of Tasks (Power intensive/Non Power intensive) for Both Profiling Methods, All Metrics and Power :
```commandline
python3 plot_perforamnce_metrics_wrt_tolerance.py \
	--logdir-loc <path-to-directories> \
	--logdirprefix <common prefix for log directories> \
	--out-filename out.txt \
	--plot-wait-time --plot-execution-time --plot-power-tolerance --plot-all \
	--power-intensive-tasks \
	--title "title"
```

* Command to obtain a Graph for All Tasks ,for Both Profiling Methods, All Metrics and Power :
```commandline
python3 plot_perforamnce_metrics_wrt_tolerance.py \
	--logdir-loc <path-to-directories>
	--logdirprefix <common prefix for log directories> \
	--out-filename out.txt \
	--all-tasks \
	--plot-wait-time --plot-execution-time  --plot-all \
	--title "title"
```

# Docker Instructions

### [Docker URL](https://hub.docker.com/r/kunal2795/perf-metrics)
```
docker pull kunal2795/perf-metrics
```

### Creating Docker Image
```
docker build . -t <Tag-Name> --no-cache
```

### To run the image
```
docker run -v <Path-to-input>:/tmp/ -it <Image-Tag-Name> <Arguments> <output-path-for-graphs>
```

Example Command:
```
docker run -v /c/Users/Kunal/Inputs:/tmp/ -it perf-metrics:latest --logdirprefix medmed --logdir-loc /tmp/no-alignment-specialtasks-crypto-video 
--out-filename /tmp/out121.txt --title "HeT-LeT" --plot-execution-time --plot-wait-time --critical-tasks --tasks-group 
--ffbp-loc /tmp/Electron-FF-medmedmedpu_2019-March-15_19-19-24/ /tmp/Electron-BP-medmedmedpu_2019-March-27_10-32-47/ --output-path /tmp/
```


## Instructions to acquire the Statistics using only "compare_stats.py":
```
usage: python3 compare_stats.py [-h] [--dir [DIR [DIR ...]]]
                        [--files [FILES [FILES ...]]] [--dir_out DIR_OUT]
                        [--output_file OUTPUT_FILE] [--task TASK]
                        [--powerclass] [--png PNG] [--et] [--wt] [--tat]
                        [--profile PROFILE][--tasks [TASKS [TASKS ...]]]
                        [--set_output SET_OUTPUT]
Files for computing the average

optional arguments:
  -h, --help            show this help message and exit
  --dir [DIR [DIR ...]]
                        Multiple directory paths to compute tolerance
  --files [FILES [FILES ...]]
                        Multiple file paths to compute average
  --dir_out DIR_OUT     Output file to print results for the medmedmedpu and medmedmaxpeakpu methods
  --output_file OUTPUT_FILE
                        Output file to print results
  --task TASK           Plot data for the given task name
  --powerclass          Plot for power classes and wait time
  --png PNG             Name of PNG file to save in
  --et                  Tolerance vs Execution time plot
  --wt                  Tolerance vs Wait time plot
  --tat                 Tolerance vs Turnaround time plot
  --profile PROFILE     Options- medmedmedpu or medmedmaxpeakpu
  --tasks [TASKS [TASKS ...]]
                          Plot data for given set of tasks
  --set_output SET_OUTPUT
                          Output file to print results of set of tasks
```
### When using the --dir option
```
Directory paths to be entered will have only two keys - "medmedmedpu" or "medmedmaxpeakpu".

Format to be followed for provding the directory paths:-
medmedmedpu=Electron-WaaR-medmedmedpu-<xxx>-20perctolerance_<timestamp>/
medmedmedpu=Electron-WaaR-medmedmedpu-<xxx>-10perctolerance_<timestamp>/
medmedmedpu=Electron-WaaR-medmedmedpu-<xxx>-30perctolerance_<timestamp>/
medmedmaxpeakpu=Electron-WaaR-medmedmaxpeakpu-<xxx>-10perctolerance_<timestamp>/
medmedmaxpeakpu=Electron-WaaR-medmedmaxpeakpu-<xxx>-20perctolerance_<timestamp>/
medmedmaxpeakpu=Electron-WaaR-medmedmaxpeakpu-<xxx>-30perctolerance_<timestamp>/
```
* Command to run using --dir:
```
$ python3 compare_stats.py --dir medmedmedpu=<directory_path> medmedmaxpeakpu=<directory_path> --dir_out "output_file"
```

* Command to run for Overall Tolerance Plots comparing both profiling methods for All Tasks:
```
This command would generate 3 plots for Tolerance vs (Execution Time, Wait Time, Turnaround Time)

$ python3 compare_stats.py --dir medmedmedpu=Electron-WaaR-medmedmedpu-cmw-alignScoreWeighted-100perctolerance_2019-February-24_21-50-2/ medmedmedpu=Electron-WaaR-medmedmedpu-cmw-alignScoreWeighted-80perctolerance_2019-February-25_3-21-22/ medmedmaxpeakpu=Electron-WaaR-medmedmaxpeakpu-cmw-alignScoreWeighted-100perctolerance_2019-February-24_4-17-6/ medmedmaxpeakpu=Electron-WaaR-medmedmaxpeakpu-cmw-alignScoreWeighted-80perctolerance_2019-February-25_0-38-16/ --dir_out out1.txt --et --wt --tat

```
* Command for Tolerance Plots comparing both profiling methods for a Specific Task:
```
This command would generate 3 plots for Tolerance vs (Execution Time, Wait Time, Turnaround Time)

$ python3 compare_stats.py --dir medmedmedpu=Electron-WaaR-medmedmedpu-cmw-alignScoreWeighted-100perctolerance_2019-February-24_21-50-2/ medmedmedpu=Electron-WaaR-medmedmedpu-cmw-alignScoreWeighted-80perctolerance_2019-February-25_3-21-22/ medmedmaxpeakpu=Electron-WaaR-medmedmaxpeakpu-cmw-alignScoreWeighted-100perctolerance_2019-February-24_4-17-6/ medmedmaxpeakpu=Electron-WaaR-medmedmaxpeakpu-cmw-alignScoreWeighted-80perctolerance_2019-February-25_0-38-16/ --dir_out out1.txt --task "electron-minife" --et --wt --tat

```

* Command for Tolerance Plots comparing both profiling methods for a Set of Tasks:
```
Tasks to be provided after --tasks option eg. --tasks electron-minife electron-jython...
Output file also to be provided for the set of tasks. Use --set_output <output-file>

This command would generate 3 plots for Tolerance vs (Execution Time, Wait Time, Turnaround Time):

$ python3 compare_stats.py --dir medmedmedpu=/home/kkulkar3/Electron-WaaR-medmedmedpu-cmw-alignScoreWeighted-100perctolerance_2019-February-24_21-50-2/ medmedmedpu=/home/kkulkar3/Electron-WaaR-medmedmedpu-cmw-alignScoreWeighted-80perctolerance_2019-February-25_3-21-22/ medmedmaxpeakpu=/home/kkulkar3/Electron-WaaR-medmedmaxpeakpu-cmw-alignScoreWeighted-100perctolerance_2019-February-24_4-17-6/ medmedmaxpeakpu=/home/kkulkar3/Electron-WaaR-medmedmaxpeakpu-cmw-alignScoreWeighted-80perctolerance_2019-February-25_0-38-16/ --dir_out outt.txt --tasks electron-minife electron-jython --set_output setout.txt --et --wt --tat

```
* Command for generating Graph for only one profiling method for All Tasks:
```
The directory paths would stay the same as above.
--profile PROFILE option to be used after providing the output_file.
Only two keys accepted - "medmedmedpu" OR "medmedmaxpeakpu"
This would generate either a "medmedmedpu_averages.html" or "medmedmaxpeakpu_averages.html" file.

Sample Command:

$ python3 compare_stats.py --dir medmedmedpu=/home/kkulkar3/Electron-WaaR-medmedmedpu-cmw-alignScoreWeighted-100perctolerance_2019-February-24_21-50-2/ medmedmedpu=/home/kkulkar3/Electron-WaaR-medmedmedpu-cmw-alignScoreWeighted-80perctolerance_2019-February-25_3-21-22/ medmedmaxpeakpu=/home/kkulkar3/Electron-WaaR-medmedmaxpeakpu-cmw-alignScoreWeighted-100perctolerance_2019-February-24_4-17-6/ medmedmaxpeakpu=/home/kkulkar3/Electron-WaaR-medmedmaxpeakpu-cmw-alignScoreWeighted-80perctolerance_2019-February-25_0-38-16/ --dir_out output.txt --profile medmedmedpu

```
* Command to generate graph for only one profiling method for a Set of Tasks:
```
Possible values after --profile - "medmedmedpu" OR "medmedmaxpeakpu"
Output file also to be provided for the set of tasks. Use --set_output <output-file>

$ python3 compare_stats.py --dir medmedmedpu=/home/kkulkar3/Electron-WaaR-medmedmedpu-cmw-alignScoreWeighted-100perctolerance_2019-February-24_21-50-2/ medmedmedpu=/home/kkulkar3/Electron-WaaR-medmedmedpu-cmw-alignScoreWeighted-80perctolerance_2019-February-25_3-21-22/ medmedmaxpeakpu=/home/kkulkar3/Electron-WaaR-medmedmaxpeakpu-cmw-alignScoreWeighted-100perctolerance_2019-February-24_4-17-6/ medmedmaxpeakpu=/home/kkulkar3/Electron-WaaR-medmedmaxpeakpu-cmw-alignScoreWeighted-80perctolerance_2019-February-25_0-38-16/ --dir_out outt.txt --tasks electron-minife electron-jython --set_output setout.txt --profile medmedmedpu

```

### When using the --files option :
```
RuntimeMetrics and SchedTrace logs are required files for each run and both files belonging to the same run should have the same key.

Format to be followed for providing the RuntimeMetrics and SchedTrace files:-
key1=Run1_runtimeMetrics.log key1=Run1_schedTrace.log key2=Run2_runtimeMetrics.log key2=schedTrace.log...
```

* Command to run the script with Runtime and SchedTrace files:
```
$ python3 compare_stats.py --files <runtimeFile> <schedtraceFile> --output_file <output_file>
```
* Command to run the script with plot for Overalls of Files:
```
$ python3 compare_stats.py --files r1=Electron-RMD-Test-Run1_20190123192226_runtimeMetrics.log r1=Electron-RMD-Test-Run1_20190123192226_schedTrace.log r2=Electron-RMD-Test-Run2_20190124193501_runtimeMetrics.log r2=Electron-RMD-Test-Run2_20190124193501_schedTrace.log --output_file out

```
* Command to run the script with plot for Averages of a specific Task with Files
```
$ python3 compare_stats.py --files r1=Electron-RMD-Test-Run1_20190123192226_runtimeMetrics.log r1=Electron-RMD-Test-Run1_20190123192226_schedTrace.log r2=Electron-RMD-Test-Run2_20190124193501_runtimeMetrics.log r2=Electron-RMD-Test-Run2_20190124193501_schedTrace.log --output_file out --task "electron-minife"
```
* Command to run the script with plot for Averages of Power Classes of a specific Task with Files
```
$ python3 compare_stats.py --files r1=Electron-RMD-Test-Run1_20190123192226_runtimeMetrics.log r1=Electron-RMD-Test-Run1_20190123192226_schedTrace.log r2=Electron-RMD-Test-Run2_20190124193501_runtimeMetrics.log r2=Electron-RMD-Test-Run2_20190124193501_schedTrace.log --output_file out --task "electron-minife" --powerclass
```


## Instructions to obtain the Variance:

Population Variance used.

usage: python variance_stats.py [-h] [--runTimeMetricsFile RUNTIMEMETRICSFILE]
                         [--schedTraceFile SCHEDTRACEFILE]
                         [--output_file OUTPUT_FILE]

Files for variance in Execution and Waiting Time

optional arguments:
  -h, --help            show this help message and exit
  --runTimeMetricsFile RUNTIMEMETRICSFILE
                        runtimeMetrics.log file
  --schedTraceFile SCHEDTRACEFILE
                        schedTrace.log file
  --output_file OUTPUT_FILE
                        Text file to display variance in WTs
