#!/usr/bin/python

import argparse
import sys
import os

# default command string.
BASE_CMD_STRING = "python3 compare_stats.py"

class Command:

    def __init__(self, cmd_builder):
        self.cmd_builder = cmd_builder

    def build(self, args):
        # Using command-line arguments to build the command string.

        #output path
        if args.output_path:
            self.cmd_builder._with_output_path(args.output_path)
        # profile method.
        if args.profile_method:
            self.cmd_builder._with_profile_method(args.profile_method)

        # log directories.
        self.cmd_builder._with_logdirs(args.logdirprefix, args.logdir_loc, args.profile_method)

        if args.ffbp_loc:
            self.cmd_builder._with_ffbp_dirs(args.ffbp_loc)
        # power-intensive | non-power-intensive | all tasks
        if args.power_intensive_tasks and args.non_power_intensive_tasks:
            print("ERROR: require only one of --power-intensive-tasks or --non-power-intensive-tasks. Got both!")
            sys.exit()
        if args.power_intensive_tasks and not args.tasks_group:
            self.cmd_builder._with_power_intensive_tasks()

        elif args.non_power_intensive_tasks and not args.tasks_group:
            self.cmd_builder._with_non_power_intensive_tasks()

        elif args.low_et_tasks and not args.tasks_group:
            self.cmd_builder._with_low_et_tasks()

        elif args.high_et_tasks and not args.tasks_group:
            self.cmd_builder._with_high_et_tasks()

        #For single task vs Rest
        if args.tasks_group:
            self.cmd_builder._with_grouped_tasks(args.power_intensive_tasks,args.low_et_tasks,args.critical_tasks)

        # Plot power vs tolerance for all alignments.
        if args.plot_align:
            self.cmd_builder._with_align_plot()

        # execution time to be plotted.
        if args.plot_execution_time:
            self.cmd_builder._with_execution_time()

        # wait time to be plotted.
        if args.plot_wait_time:
            self.cmd_builder._with_wait_time()

        # plot Tolerance vs power
        if args.plot_power_tolerance:
            self.cmd_builder._with_power_tolerance()

        # power profile percentile.
        self.cmd_builder._with_power_profile_percentile(args.power_profile_percentile)

        # plot Tolerance vs power vs Time
        if args.plot_all:
            self.cmd_builder._with_plot_all()

        # plot Tolerance vs power vs Time
        if args.title:
            self.cmd_builder._with_title_graph(args.title)

        # output filename.
        self.cmd_builder._with_output_filename(args.out_filename,args.power_intensive_tasks,args.non_power_intensive_tasks)

        # string representation of watts tolerance in electron log filename.
        if args.wtol_usage_in_filename:
            self.cmd_builder._with_wtol_usage_in_filename(args.wtol_usage_in_filename)

        # prefix before wtol specification in filename.
        self.cmd_builder._with_prefix_before_wtol_in_filename(args.prefix_before_wtol_in_filename)

        # number of powerclasses to consider when calculating stats and plotting graphs.
        if args.num_powerclasses:
            self.cmd_builder._with_num_powerclasses(args.num_powerclasses)

        # Plot for power classes.
        if args.powerclass:
            self.cmd_builder._with_powerclass()

        # BUILDING THE COMMAND STRING.
        # ALL COMMAND PARTS SHOULD BE BUILT BEFORE THIS.
        self.cmd_string = " ".join([BASE_CMD_STRING, self.cmd_builder.cmd_string()])

    def run(self):
        os.system(self.cmd_string)


# command-line options.
TASKS_CMDLINE_OPTION = "--tasks"
EXECUTION_TIME_CMDLINE_OPTION = "--et"
WAIT_TIME_CMDLINE_OPTION = "--wt"
PROFILE_METHOD_CMDLINE_OPTION = "--profile"
OUTPUT_FILENAME_CMDLINE_OPTION = "--set_output"
DIR_CMDLINE_OPTION = "--dir"
DIR_OUT_CMDLINE_OPTION = "--dir_out"
POWER_TOLERANCE_OPTION="--power"
PLOT_ALL_OPTION="--plot_all"
GRAPH_TITLE_OPTION="--graph_title"
TASKS_GROUP="--tasks_group"
PLOT_ALL_ALIGN_OPTION="--plot_power_tol_align"
FF_BP_OPTION="--ff_bp"
OUTPUT_PATH_OPTION="--output_path"
WATTS_TOLERANCE_USAGE_IN_FILENAME_OPTION="--wtol_usage_in_filename"
PREFIX_BEFORE_WTOL_IN_FILENAME_OPTION="--prefix_before_wtol_in_filename"
NUM_POWERCLASSES_OPTION="--num_powerclasses"
POWERCLASS_OPTION="--powerclass"
POWER_PROFILE_PERCENTILE_OPTION="--power_profile_percentile"

class CommandBuilder:
    """
    Building the command string with the required command-line options.
    """
    def __init__(self):
        self.parts = [] # Parts of command-line options that are later joined to form the cmd string.
        self.power_intensive_tasks = [
                "electron-cryptography",
                "electron-xalan",
                "electron-video-encoding",
                "electron-tomcat",
                "electron-dgemm",
                "electron-lusearch",
                "electron-sunflow"
        ]

        self.non_power_intensive_tasks = [
                "electron-minife",
                "electron-stream",
                "electron-jython",
                "electron-h2",
                "electron-batik",
                "electron-avrora",
                "electron-eclipse",
                "electron-fop",
                "electron-luindex",
                "electron-pmd",
                "electron-audio-encoding",
                "electron-tradebeans",
                "electron-network-loopback"
        ]
        self.low_et_tasks=[
        "electron-minife",
        "electron-stream",
        "electron-jython",
        "electron-batik",
        "electron-avrora",
        "electron-fop",
        "electron-luindex",
        "electron-lusearch",
        "electron-pmd",
        "electron-sunflow",
        "electron-tomcat",
        "electron-network-loopback"
        ]
        self.high_et_tasks=[
        "electron-dgemm",
        "electron-h2",
        "electron-eclipse",
        "electron-tradebeans",
        "electron-xalan",
        "electron-audio-encoding" ,
        "electron-video-encoding",
        "electron-cryptography"
        ]
        self.critical_tasks=[
        "electron-video-encoding",
        "electron-cryptography"
        ]
    def _with_power_intensive_tasks(self):
        self.parts.append(" ".join([TASKS_CMDLINE_OPTION, " ".join(self.power_intensive_tasks)]))

    def _with_non_power_intensive_tasks(self):
        self.parts.append(" ".join([TASKS_CMDLINE_OPTION, " ".join(self.non_power_intensive_tasks)]))

    def _with_low_et_tasks(self):
        self.parts.append(" ".join([TASKS_CMDLINE_OPTION, " ".join(self.low_et_tasks)]))

    def _with_high_et_tasks(self):
        self.parts.append(" ".join([TASKS_CMDLINE_OPTION, " ".join(self.high_et_tasks)]))

    def _with_execution_time(self):
        self.parts.append(EXECUTION_TIME_CMDLINE_OPTION)

    def _with_wait_time(self):
        self.parts.append(WAIT_TIME_CMDLINE_OPTION)

    def _with_power_tolerance(self):
        self.parts.append(POWER_TOLERANCE_OPTION)

    def _with_power_profile_percentile(self, percentile):
        self.parts.append(" ".join([POWER_PROFILE_PERCENTILE_OPTION, str(percentile)]))

    def _with_align_plot(self):
        self.parts.append(PLOT_ALL_ALIGN_OPTION)

    def _with_plot_all(self):
        self.parts.append(PLOT_ALL_OPTION)

    def _with_grouped_tasks(self,power_intensive,low_et,critical_tasks):
        if power_intensive:
            tasks=' '.join(self.power_intensive_tasks)
        elif low_et:
            tasks=' '.join(self.low_et_tasks)
        elif critical_tasks:
            tasks=' '.join(self.critical_tasks)

        self.parts.append(" ".join([TASKS_GROUP,tasks]))

    def _with_title_graph(self, title):
        self.parts.append(" ".join([GRAPH_TITLE_OPTION, title]))

    def _with_profile_method(self, method_name):
        self.parts.append(" ".join([PROFILE_METHOD_CMDLINE_OPTION, method_name]))

    def _with_output_filename(self, output_filename,power_intensive,non_power_intensive):
        if power_intensive:
            self.parts.append(" ".join([OUTPUT_FILENAME_CMDLINE_OPTION, "Power-intensive-"+output_filename]))
        if non_power_intensive:
            self.parts.append(" ".join([OUTPUT_FILENAME_CMDLINE_OPTION, "Non-Power-intensive-"+output_filename]))
        self.parts.append(" ".join([DIR_OUT_CMDLINE_OPTION, output_filename]))

    def _with_logdirs(self, logdirprefix, logdir_loc, profile_method):
        plot_dirs = [DIR_CMDLINE_OPTION]
        for root, dirs, files in os.walk(logdir_loc):
            for dirname in dirs:
                if logdirprefix in dirname:
                    plot_dirs.append("=".join([profile_method, os.path.join(root, dirname+"/")]))
        self.parts.append(" ".join(plot_dirs))

    def _with_ffbp_dirs(self,dir_paths):
        dir_paths=' '.join(dir_paths)
        self.parts.append(" ".join([FF_BP_OPTION,dir_paths]))

    def cmd_string(self):
        print(" ".join(self.parts))
        return " ".join(self.parts)

    def _with_output_path(self,path):
        self.parts.append(" ".join([OUTPUT_PATH_OPTION,path]))

    def _with_wtol_usage_in_filename(self, wtol_usage_in_filename):
        self.parts.append(" ".join([WATTS_TOLERANCE_USAGE_IN_FILENAME_OPTION, wtol_usage_in_filename]))

    def _with_prefix_before_wtol_in_filename(self, prefix_before_wtol_in_filename):
        self.parts.append(" ".join([PREFIX_BEFORE_WTOL_IN_FILENAME_OPTION, prefix_before_wtol_in_filename]))

    def _with_num_powerclasses(self, num_powerclasses):
        self.parts.append(" ".join([NUM_POWERCLASSES_OPTION, str(num_powerclasses)]))

    def _with_powerclass(self):
        self.parts.append(POWERCLASS_OPTION)

def main(args):
    cmd = Command(CommandBuilder())
    cmd.build(args)
    cmd.run()

if __name__ == "__main__":
    parser=argparse.ArgumentParser(description="Plot performance metrics against tolerance.")
    parser.add_argument("--power-intensive-tasks", help="Plot performance metrics for power-intensive tasks.", required=False, action='store_true')
    parser.add_argument("--non-power-intensive-tasks", help="Plot performance metrics for non power-intensive tasks.", required=False, action='store_true')
    parser.add_argument("--low-et-tasks", help="Plot performance metrics for Low Execution Time tasks.", required=False, action='store_true')
    parser.add_argument("--critical-tasks", help="Plot performance metrics for Critical tasks.", required=False, action='store_true')
    parser.add_argument("--high-et-tasks", help="Plot performance metrics for High Execution Time Tasks.", required=False, action='store_true')
    parser.add_argument("--all-tasks", help="Plot performance metrics for all tasks.", required=False, action='store_true')
    parser.add_argument("--profile-method", help="Method used to estimate watts for tasks. This will be used as the key.", required=False, default="medmedmaxpeakpu")
    parser.add_argument("--plot-execution-time", help="Plot execution time vs tolerance.", required=False, action='store_true')
    parser.add_argument("--plot-wait-time", help="Plot wait time vs tolerance.", required=False, action='store_true')
    parser.add_argument("--plot-power-tolerance", help="Plot Tolerance vs Power", required=False, action='store_true')
    parser.add_argument("--power-profile-percentile", help="Percentile of the peak power consumption to plot", required=False, default=90)
    parser.add_argument("--plot-align", help="Plot for all Alignments", required=False, action='store_true')
    parser.add_argument("--plot-all", help="Plot Tolerance vs Power vs Time", required=False, action='store_true')
    parser.add_argument("--tasks-group", help="Task to plot vs Rest", required=False,action='store_true')
    parser.add_argument("--title", help="Title for graph", required=False)
    parser.add_argument("--out-filename", help="Filename to store results in.", required=True)
    parser.add_argument("--logdir-loc", help="Path to directory containing log directories.", required=True)
    parser.add_argument("--ffbp-loc", help="Path to directory containing log directories.", required=False,nargs='*')
    parser.add_argument("--logdirprefix", help="Prefix common to all log directories being used to plot.", required=True)
    parser.add_argument("--output-path",help="Path for html files",required=False)
    parser.add_argument("--wtol-usage-in-filename", help="String used to represent Watts Tolerance in Electron log filenames.", required=False)
    parser.add_argument("--prefix-before-wtol-in-filename", help="Prefix used prior to watts tolerance specification in filename.", required=False, default="-")
    parser.add_argument("--num-powerclasses",help="Number of power classes", type=int, required=False, default="4")
    parser.add_argument("--powerclass",help="Plot for power classes and wait time", required=False, action="store_true")
    args=parser.parse_args()

    main(args)
